===============================================================================
DESCRIPTION
===============================================================================

This program started out as a rube-goldberg machine for my Game Math & Physics
class. The Professor told me that we would be graded primarily on our "theme"
and humor. I didn't have any humorous ideas, so I decided to put all of my eggs
into the theme basket by using lights and dynamic shadows. The only
requirements for the machine were that it starts with a rolling ball, and it
ends with a button being depressed that triggers a sound effect.

Because I was pressed for time, the quality of the code was not good. I
pulled source code from my other projects to expedite development, and that
resulted in spaghetti code. I want to use my free time to make the source
code look good so that this will be a program that I can be proud of.

To create the shadows, I used a method based on the one found here:
http://archive.gamedev.net/archive/reference/programming/features/2dsoftshadow/

It's based on that method, but it's slightly different. The difference came
about because I wanted to generate a shadow map in a single draw call where
his method uses a separate draw call for each light. In my program, the shadows
from the three light sources are rendered into different color channels. The
shadows from the first light source are rendered into the red channel, the
shadows from the second are rendered into the blue channel, and the shadows
from the third are rendered into the green channel. The texture shader reads
those values from the shadow map, and then it and combines them to determine
the light intensity of each texel.

===============================================================================
VIDEOS
===============================================================================

You can see the program in action here:
https://www.youtube.com/watch?v=URm5DL9jTbk

You can see it running in debug mode here. You can't see the dominoes falling
over in the other video:
https://www.youtube.com/watch?v=XCoGBpiaGT0

===============================================================================
COMPILING
===============================================================================

This program has three dependencies:
Directx SDK
Box2d 2.3.0
irrKlang 1.5.0

The Visual Studio project expects both Box2d and irrKlang to be within a 'deps'
directory inside of the solution directory. The DirectX SDK directory is
expected to be defined in an environmental variable named 'DXSDK_DIR'.