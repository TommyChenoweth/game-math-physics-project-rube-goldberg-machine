#pragma once

#include <d3dx9math.h>

struct point_light
{
	/*
	I consolidated the position and radius into a single float4 array 
	so that the values can be easily passed to the shader with
	SetRawValue(). Consider this excerpt from msdn:
	"All values are expected to be either matrix4x4s or float4s and all 
	matrices are expected to be in column-major order. Int or float 
	values are cast into a float4; therefore, it is highly 
	recommended that you use SetRawValue with only float4 or matrix4x4
	data."
	*/
	D3DXVECTOR4 pos_and_radius;
	//D3DXVECTOR3 position;
	//float		radius;
};