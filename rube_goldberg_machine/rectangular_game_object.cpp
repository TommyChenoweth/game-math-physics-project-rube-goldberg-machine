#include "rectangular_game_object.hpp"

game_object::hull_type rectangular_game_object::_do_get_hull_type() const
{
	return game_object::hull_type::convex_polygon;
}

convex_hull rectangular_game_object::_do_get_convex_hull() const
{
	convex_hull hull;

	auto transformation = _physics_body->GetTransform();
	auto shape = static_cast<b2PolygonShape*>(_physics_body->GetFixtureList()->GetShape());
	auto number_of_vertices = shape->GetVertexCount();
	for(unsigned i = 0; i < number_of_vertices; ++i)
	{
		auto vertex = b2Mul(transformation, shape->GetVertex(i));
		hull.emplace_back(vertex.x, vertex.y, 0.0f);
	}

	return hull;
}

circle_hull rectangular_game_object::_do_get_circle_hull() const
{
	return circle_hull{D3DXVECTOR3(0.0f, 0.0f, 0.0f), 0.0f};
}

b2BodyDef rectangular_game_object::_create_body_definition(const D3DXVECTOR3& position)
{
	b2BodyDef body_def;
	body_def.type = b2_dynamicBody;
	body_def.position.Set(position.x, position.y);
	return body_def;
}

b2PolygonShape rectangular_game_object::_create_shape(const float half_width, const float half_height)
{
	b2PolygonShape rectangle;
	rectangle.SetAsBox(half_width, half_height);
	return rectangle;
}

b2FixtureDef rectangular_game_object::_create_fixture_definition(const b2PolygonShape& shape)
{
	b2FixtureDef fixture_def;
	fixture_def.shape = &shape;
	fixture_def.density = 1.0f;
	return fixture_def;
}

b2Body* rectangular_game_object::_create_physics_body(const D3DXVECTOR3& position, const float half_width, const float half_height, b2World* world)
{
	b2Body* body(nullptr);
	b2BodyDef body_def = _create_body_definition(position);
	body = world->CreateBody(&body_def);

	b2PolygonShape rectangle_shape = _create_shape(half_width, half_height);

	b2FixtureDef fixture_def = _create_fixture_definition(rectangle_shape);
	body->CreateFixture(&fixture_def);

	return body;
}

b2Body* rectangular_game_object::_do_get_physics_body()
{
	return _physics_body;
}

D3DXVECTOR3 rectangular_game_object::_do_get_position() const
{
	auto physics_body_center_position = _physics_body->GetPosition();
	D3DXVECTOR3 bottom_left_position(physics_body_center_position.x, physics_body_center_position.y, 0.0f);
	bottom_left_position -= D3DXVECTOR3(_half_width, _half_height, 0.0f);
	return bottom_left_position;
}

convex_hull rectangular_game_object::_create_convex_hull(const D3DXVECTOR3& position, const float width, const float height)
{
	const D3DXVECTOR3 y_axis(0.0f, 1.0f, 0.0f);
	const D3DXVECTOR3 x_axis(1.0f, 0.0f, 0.0f);

	D3DXVECTOR3 bottom_left(position);
	D3DXVECTOR3 top_left = bottom_left + height*y_axis;
	D3DXVECTOR3 bottom_right = bottom_left + width*x_axis;
	D3DXVECTOR3 top_right = bottom_right + height*y_axis;

	convex_hull hull;
	hull.push_back(bottom_left);
	hull.push_back(bottom_right);
	hull.push_back(top_right);
	hull.push_back(top_left);
	return hull;
}

rectangular_game_object::rectangular_game_object(const D3DXVECTOR3& position, b2World* world, const float half_width, const float half_height, const std::wstring& path_to_texture_file, IDirect3DDevice9* d3d_device)
	: game_object(path_to_texture_file, d3d_device),
	  _physics_body(nullptr),
	  _half_width(half_width),
	  _half_height(half_height)
{
}

void rectangular_game_object::rebuild_physics_body(const D3DXVECTOR3& position, const float half_width, const float half_height, b2World* world)
{
	if(_physics_body)
	{
		world->DestroyBody(_physics_body);
		_physics_body = nullptr;
	}

	_physics_body = _create_physics_body(position, half_width, half_height, world);
}