#pragma once

#include "rectangular_game_object.hpp"

class domino : public rectangular_game_object
{
private:
	RECT _render_rect;
	float _width;
	float _height;

	virtual b2FixtureDef _create_fixture_definition(const b2PolygonShape& shape);
	virtual const RECT* _do_get_render_rect() const;

public:
	domino(const D3DXVECTOR3& position, b2World* world, const float pixels_per_meter, const float width, const float height, IDirect3DDevice9* d3d_device);

};