#include "peg.hpp"

#include <cmath>

game_object::hull_type peg::_do_get_hull_type() const
{
	return game_object::hull_type::convex_polygon;
}

convex_hull peg::_do_get_convex_hull() const
{
	convex_hull hull;

	auto pos = _do_get_position();
	D3DXMATRIX translate;
	D3DXMatrixTranslation(&translate, pos.x, pos.y, 0.0f);
	for(const auto& v : _hull)
	{
		D3DXVECTOR3 transformed_vertex;
		D3DXVec3TransformCoord(&transformed_vertex, &v, &translate);
		hull.push_back(transformed_vertex);
	}

	return hull;
}

circle_hull peg::_do_get_circle_hull() const
{
	return{D3DXVECTOR3(0.0f, 0.0f, 0.0f), 0.0f};
}

b2Body* peg::_do_get_physics_body()
{
	return _physics_body;
}

D3DXVECTOR3 peg::_do_get_position() const
{
	auto b2d_pos = _physics_body->GetPosition();
	D3DXVECTOR3 d3d_position(b2d_pos.x, b2d_pos.y, _position.z);
	return d3d_position;
}

const RECT* peg::_do_get_render_rect() const
{
	return nullptr;
}

peg::peg(const D3DXVECTOR3& position, b2World* world, IDirect3DDevice9* d3d_device)
: game_object(L"../textures/peg.dds", d3d_device),
_physics_body(nullptr),
_position(position),
_radius(1.0f/64.0f*4.0f)
{
	b2BodyDef body_def;
	body_def.type = b2_staticBody;
	body_def.position.Set(position.x, position.y);
	_physics_body = world->CreateBody(&body_def);

	b2CircleShape shape;
	shape.m_radius = _radius;

	b2FixtureDef fixture_def;
	fixture_def.restitution = 1.0f;
	fixture_def.shape = &shape;

	_physics_body->CreateFixture(&fixture_def);

	const int resolution = 10;
	for(int i = 0; i < resolution; ++i)
	{
		D3DXVECTOR3 v(0.0f, 0.0f, 0.0f);

		auto theta = i * (2 * D3DX_PI / resolution);
		v.x = std::cos(theta);
		v.y = std::sin(theta);

		v *= _radius;

		_hull.push_back(v);
	}
}