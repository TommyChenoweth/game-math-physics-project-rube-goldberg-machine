#pragma once

#include "game_object.hpp"
#include "point_light.hpp"
#include "sgr_vertex.hpp"

#include <array>
#include <d3dx9.h>
#include <memory>
#include <string>
#include <vector>

class shadow_geometry_renderer
{
private:
	std::vector<sgr_vertex>	_shadow_quad_vertices;

	std::shared_ptr<IDirect3DDevice9>	_d3d_device;

	std::shared_ptr<IDirect3DVertexBuffer9>		_vertex_buffer;
	std::shared_ptr<IDirect3DIndexBuffer9>		_index_buffer;
	unsigned									_vertex_buffer_index;
	static const unsigned						_QUAD_CAPACITY = 1000;
	static const unsigned						_VERTICES_PER_QUAD = 4;
	static const unsigned						_INDICES_PER_QUAD = 6;
	static const unsigned						_TRIS_PER_QUAD = 2;

	std::shared_ptr<ID3DXEffect>		_effect;
	std::wstring						_effect_file_path;
	D3DXHANDLE							_effect_handle;
	D3DXHANDLE							_effect_vp_handle;

	float								_meters_per_pixel;

	std::vector<int> _tri_count;

	void _create_vertex_buffer();
	void _create_index_buffer();

	void _create_effect();
	void _load_effect_handle();

	void _create_vertices_for_quads();
	void _create_indices_for_quads();

	void _generate_shadow_geometry_for_convex_hull(const point_light& light, const convex_hull& hull, const D3DXVECTOR4& color);
	void _generate_shadow_geometry_for_circle_hull(const point_light& light, const circle_hull& hull, const D3DXVECTOR4& color);

public:
	shadow_geometry_renderer(std::shared_ptr<IDirect3DDevice9> d3d_device);

	void generate_shadows_for_hulls(const point_light& light, int index, std::vector<game_object*> gos);

	void render(const D3DXMATRIX& view_projection);
};