#pragma once

#include "game_object.hpp"

#include <Box2D\Box2D.h>

class rectangular_game_object : public game_object
{
private:
	b2Body*		_physics_body;
	float		_half_width;
	float		_half_height;

	virtual game_object::hull_type _do_get_hull_type() const;
	virtual convex_hull _do_get_convex_hull() const;
	virtual circle_hull _do_get_circle_hull() const;
	virtual b2Body* _do_get_physics_body();
	virtual D3DXVECTOR3 _do_get_position() const;

	virtual b2BodyDef _create_body_definition(const D3DXVECTOR3& position);
	virtual b2PolygonShape _create_shape(const float half_width, const float half_height);
	virtual b2FixtureDef _create_fixture_definition(const b2PolygonShape& shape);
	virtual b2Body* _create_physics_body(const D3DXVECTOR3& position, const float half_width, const float half_height, b2World* world);
	virtual convex_hull _create_convex_hull(const D3DXVECTOR3& position, const float width, const float height);

public:
	rectangular_game_object(const D3DXVECTOR3& position, b2World* world, const float half_width, const float half_height, const std::wstring& path_to_texture_file, IDirect3DDevice9* d3d_device);

	void rebuild_physics_body(const D3DXVECTOR3& position, const float half_width, const float half_height, b2World* world);

};