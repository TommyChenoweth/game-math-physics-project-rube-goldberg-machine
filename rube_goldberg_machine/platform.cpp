#include "platform.hpp"

#include <Box2D\Box2D.h>

b2BodyDef platform::_create_body_definition(const D3DXVECTOR3& position)
{
	b2BodyDef body_def;
	body_def.type = b2_staticBody;
	body_def.position.Set(position.x + _width/2, position.y + _height/2);
	return body_def;
}

const RECT* platform::_do_get_render_rect() const
{
	return &_render_rect;
}

platform::platform(const D3DXVECTOR3& position, b2World* world, const float pixels_per_meter, const float width, const float height, IDirect3DDevice9* d3d_device)
: rectangular_game_object(position, world, width / 2, height / 2, L"../textures/platform.dds", d3d_device),
_width(width),
_height(height)
{
	_render_rect = {0, 0, width*pixels_per_meter, height*pixels_per_meter};
	rebuild_physics_body(position, width / 2, height / 2, world);
}