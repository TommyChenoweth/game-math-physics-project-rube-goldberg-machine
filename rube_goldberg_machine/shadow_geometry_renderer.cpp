#include "shadow_geometry_renderer.hpp"

#include "com_pointer.hpp"
#include "debug.hpp"
#include "sgr_vertex.hpp"
#include "tqr_vertex.hpp"

#include <algorithm>
#include <iostream>
#include <cmath>

void shadow_geometry_renderer::_create_vertex_buffer()
{
	IDirect3DVertexBuffer9* vertex_buffer(nullptr);
	HR(_d3d_device->CreateVertexBuffer(
		_QUAD_CAPACITY * _VERTICES_PER_QUAD * sizeof(sgr_vertex),
		D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
		0,
		D3DPOOL_DEFAULT,
		&vertex_buffer,
		nullptr);
	_vertex_buffer = create_com_pointer(vertex_buffer));
}

void shadow_geometry_renderer::_create_index_buffer()
{
	IDirect3DIndexBuffer9* index_buffer(nullptr);
	HR(_d3d_device->CreateIndexBuffer(
		_QUAD_CAPACITY * _INDICES_PER_QUAD * sizeof(unsigned __int16),
		0,
		D3DFMT_INDEX16,
		D3DPOOL_DEFAULT,
		&index_buffer,
		nullptr));
	_index_buffer = create_com_pointer(index_buffer);
}

void shadow_geometry_renderer::_create_effect()
{
	ID3DXEffect* effect(nullptr);
	ID3DXBuffer* error_buffer(nullptr);

	DWORD shader_flags = 0;
	#ifdef _DEBUG
		shader_flags |= D3DXSHADER_DEBUG | D3DXSHADER_SKIPOPTIMIZATION;
	#endif

	auto create_fx_result = D3DXCreateEffectFromFile(
		_d3d_device.get(),
		_effect_file_path.c_str(),
		nullptr,
		nullptr,
		shader_flags,
		nullptr,
		&effect,
		&error_buffer);
	_effect = create_com_pointer(effect);

	if(error_buffer)
	{
		std::cerr << "SHADER ERRORS: " << reinterpret_cast<char*>(error_buffer->GetBufferPointer()) << std::endl;
		error_buffer->Release();
	}

	HR(create_fx_result);
}

void shadow_geometry_renderer::_load_effect_handle()
{
	_effect_handle = _effect->GetTechniqueByName("TQRenderTech");
	_effect_vp_handle = _effect->GetParameterByName(0, "g_view_projection");
}

void shadow_geometry_renderer::_create_vertices_for_quads()
{
	DWORD lock_flags(D3DLOCK_NOOVERWRITE);

	bool enough_space_in_buffer
		= (_QUAD_CAPACITY * _VERTICES_PER_QUAD - _vertex_buffer_index < _shadow_quad_vertices.size());

	if(!enough_space_in_buffer)
	{
		lock_flags = D3DLOCK_DISCARD;
		_vertex_buffer_index = 0;
	}

	sgr_vertex* vertex(nullptr);
	HR(_vertex_buffer->Lock(
		_vertex_buffer_index,
		_shadow_quad_vertices.size() * sizeof(sgr_vertex),
		reinterpret_cast<void**>(&vertex),
		lock_flags));

	const float meters_per_pixel = _meters_per_pixel;

	// Copy the vertex buffer here.
	memcpy(vertex, &_shadow_quad_vertices[0], _shadow_quad_vertices.size()*sizeof(sgr_vertex));

	HR(_vertex_buffer->Unlock());

}

void shadow_geometry_renderer::_create_indices_for_quads()
{
	unsigned __int16* index(nullptr);
	HR(_index_buffer->Lock(
		0,
		0,
		reinterpret_cast<void**>(&index),
		0));

	for(unsigned i = 0; i < _QUAD_CAPACITY; ++i)
	{
		// This is the first triangle in the quad.
		index[(i*_INDICES_PER_QUAD) + 0] = 0 + i*_VERTICES_PER_QUAD;
		index[(i*_INDICES_PER_QUAD) + 1] = 1 + i*_VERTICES_PER_QUAD;
		index[(i*_INDICES_PER_QUAD) + 2] = 2 + i*_VERTICES_PER_QUAD;

		// This is the second triangle in the quad.
		index[(i*_INDICES_PER_QUAD) + 3] = 0 + i*_VERTICES_PER_QUAD;
		index[(i*_INDICES_PER_QUAD) + 4] = 2 + i*_VERTICES_PER_QUAD;
		index[(i*_INDICES_PER_QUAD) + 5] = 3 + i*_VERTICES_PER_QUAD;
	}

	HR(_index_buffer->Unlock());
}

void shadow_geometry_renderer::_generate_shadow_geometry_for_convex_hull(const point_light& light, const convex_hull& hull, const D3DXVECTOR4& color)
{
	bool strip_cutting = false;
	if(0 < _shadow_quad_vertices.size())
	{
		auto n_minus_1 = _shadow_quad_vertices[_shadow_quad_vertices.size() - 1];
		_shadow_quad_vertices.push_back(n_minus_1);
		strip_cutting = true;
	}

	D3DXVECTOR3 hull_center = hull[0] + 0.5f*(hull[2] - hull[0]);
	D3DXVECTOR3 light_position(light.pos_and_radius.x, light.pos_and_radius.y, light.pos_and_radius.z);

	D3DXVECTOR3 light_to_dark(0.0f, 0.0f, 0.0f);
	int light_to_dark_index(-1);
	D3DXVECTOR3 dark_to_light(0.0f, 0.0f, 0.0f);
	int dark_to_light_index(-1);

	auto edge_is_front_facing = [](D3DXVECTOR3 v1, D3DXVECTOR3 v2, D3DXVECTOR3 to_light) -> bool
	{
		auto v1_l = D3DXVec3LengthSq(&v1);
		auto v2_l = D3DXVec3LengthSq(&v2);

		float y = v2.x - v1.x;
		if(y != 0)
			y = -y;
		D3DXVECTOR3 edge_normal(v2.y - v1.y, y, 0.0f);
		bool edge_is_front_facing(0 < D3DXVec3Dot(&to_light, &edge_normal));
		return edge_is_front_facing;
	};

	bool last_edge_was_front_facing = false;
	std::vector<int> edge_front_facing(hull.size(), false);
	for(int i = 0; i < hull.size() + 1; ++i)
	{
		int i1 = i == 0 ? hull.size() - 1 : i - 1;
		int i2 = i == hull.size() ? 0 : i;

		D3DXVECTOR3 to_light = light_position - hull[i2];

		bool front_facing = edge_is_front_facing(hull[i1], hull[i2], to_light);

		if(front_facing) // Light
		{
			edge_front_facing[i2] = true;

			if(!last_edge_was_front_facing && i != 0)
			{
				dark_to_light = hull[i1];
				dark_to_light_index = i1;
			}
			last_edge_was_front_facing = true;
		}
		else // Dark
		{
			if(last_edge_was_front_facing && i != 0)
			{
				light_to_dark = hull[i1];
				light_to_dark_index = i1;
			}
			last_edge_was_front_facing = false;
		}
	}

	float light_radius = light.pos_and_radius.w;

	int starting_index = dark_to_light_index;

	int i = 0;
	int dist = std::count(edge_front_facing.begin(), edge_front_facing.end(), false) + 1;
	while(i < dist)
	{
		int index = (starting_index + -i) % hull.size();
		auto current_vertex = hull[index];

		auto away_from_light = current_vertex - light_position;
		D3DXVec3Normalize(&away_from_light, &away_from_light);
		auto behind_current_vertex = current_vertex + light_radius*away_from_light;

		_shadow_quad_vertices.emplace_back(current_vertex, color);
		if(strip_cutting)
		{
			_shadow_quad_vertices.emplace_back(current_vertex, color);
			strip_cutting = false;
		}
		_shadow_quad_vertices.emplace_back(behind_current_vertex, color);

		++i;
	}
}

void shadow_geometry_renderer::_generate_shadow_geometry_for_circle_hull(const point_light& light, const circle_hull& hull, const D3DXVECTOR4& color)
{
	D3DXVECTOR3 light_position(light.pos_and_radius.x, light.pos_and_radius.y, light.pos_and_radius.z);
	auto to_light = light_position - hull.center;

	D3DXVECTOR3 to_right;
	D3DXVec3Cross(&to_right, &to_light, &D3DXVECTOR3(0.0f, 0.0f, 1.0f));

	D3DXVec3Normalize(&to_right, &to_right);

	auto light_radius = light.pos_and_radius.w;

	to_right *= hull.radius;

	D3DXVECTOR3 to_left = -to_right;

	auto left_position = hull.center + to_left;
	auto left_direction = (left_position - light_position);
	D3DXVec3Normalize(&left_direction, &left_direction);
	auto left_side = light_radius*left_direction;
	auto back_left_position = left_position + left_side;

	auto right_position = hull.center + to_right;
	auto right_direction = right_position - light_position;
	D3DXVec3Normalize(&right_direction, &right_direction);
	auto right_side = light_radius*right_direction;
	auto back_right_position = right_position + right_side;

	_shadow_quad_vertices.emplace_back(right_position, color);
	_shadow_quad_vertices.emplace_back(back_right_position, color);
	_shadow_quad_vertices.emplace_back(back_left_position, color);
	_shadow_quad_vertices.emplace_back(left_position, color);
}

shadow_geometry_renderer::shadow_geometry_renderer(std::shared_ptr<IDirect3DDevice9> d3d_device)
	: _d3d_device(d3d_device),
	  _index_buffer(nullptr),
	  _vertex_buffer(nullptr),
	  _effect(nullptr),
	  _effect_file_path(L"../shaders/shadow_geometry.fx"),
	  _meters_per_pixel(64.0f)
{
	sgr_vertex::initialize_vertex_declaration(_d3d_device);

	_create_vertex_buffer();
	_create_index_buffer();

	_create_indices_for_quads();

	_create_effect();
	_load_effect_handle();
}

void shadow_geometry_renderer::generate_shadows_for_hulls(const point_light& light, int index, std::vector<game_object*> gos)
{
	D3DXVECTOR4 color;
	switch(index)
	{
	case 0:
		color = D3DXVECTOR4(1.0f, 0.0f, 0.0f, 1.0f);
		break;
	case 1:
		color = D3DXVECTOR4(0.0f, 1.0f, 0.0f, 1.0f);
		break;
	case 2:
		color = D3DXVECTOR4(0.0f, 0.0f, 1.0f, 1.0f);
		break;
	}

	for(auto& go : gos)
	{
		switch(go->get_hull_type())
		{
		case game_object::hull_type::convex_polygon:
			if(0 < go->get_convex_hull().size())
				_generate_shadow_geometry_for_convex_hull(light, go->get_convex_hull(), color);
			break;
		case game_object::hull_type::circle:
			_generate_shadow_geometry_for_circle_hull(light, go->get_circle_hull(), color);
			break;
		}
	}
}

void shadow_geometry_renderer::render(const D3DXMATRIX& view_projection)
{
	_create_vertices_for_quads();

	HR(_d3d_device->SetVertexDeclaration(sgr_vertex::get_vertex_declaration().get()));

	HR(_d3d_device->SetStreamSource(0, _vertex_buffer.get(), _vertex_buffer_index*sizeof(sgr_vertex), sizeof(sgr_vertex)));
	HR(_d3d_device->SetIndices(_index_buffer.get()));

	HR(_effect->SetTechnique(_effect_handle));

	HR(_effect->SetMatrix(_effect_vp_handle, &view_projection));

	UINT number_of_passes(0);
	HR(_effect->Begin(&number_of_passes, 0));

	for(unsigned current_pass = 0; current_pass < number_of_passes; ++current_pass)
	{
		HR(_effect->BeginPass(current_pass));
		HR(_d3d_device->DrawPrimitive(
			D3DPT_TRIANGLESTRIP,
			0,
			_shadow_quad_vertices.size() - 2));
		HR(_effect->EndPass());
	}

	HR(_effect->End());

	_shadow_quad_vertices.clear();
}