#pragma once

#include <Box2D/Box2D.h>
#include <d3dx9.h>
#include <memory>
#include <vector>

class b2d_debug_draw : public b2Draw
{
private:
	std::shared_ptr<ID3DXLine>	_d3d_line;
	std::vector<D3DXVECTOR3>	_vertices;
	D3DCOLOR					_color;
	D3DXMATRIX					_view_projection;

	void _b2d_to_d3d_vectors(const b2Vec2* vertices, const unsigned number_of_vertices);
	void _b2d_to_d3d_color(const b2Color& color);

public:
	b2d_debug_draw(std::shared_ptr<ID3DXLine> d3d_line);

	void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
	void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
	void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);
	void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);
	void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);
	void DrawTransform(const b2Transform& xf);

	void SetTransform(const D3DXMATRIX& transform);
};