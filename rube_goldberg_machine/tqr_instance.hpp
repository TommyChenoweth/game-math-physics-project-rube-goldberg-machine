#pragma once

#include <d3dx9.h>

/**
 *  Represents instance data of a textured quad. That is, it represents data
 *  common to all vertices of a textured quad.
 */
class tqr_instance
{
public:
	D3DXVECTOR3 quad_position;
	D3DXVECTOR4 quad_rect;

	tqr_instance(const D3DXVECTOR3& quad_position, const D3DXVECTOR4& quad_rect);
	tqr_instance();
};