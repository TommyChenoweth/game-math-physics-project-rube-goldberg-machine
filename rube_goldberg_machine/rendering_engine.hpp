#pragma once

#include "point_light.hpp"

#include <array>
#include <d3d9.h>
#include <d3dx9.h>
#include <D3dx9math.h>
#include <memory>
#include <vector>

class box2d;
class b2d_debug_draw;
class camera;
class direct3d;
class game_object;
class shadow_geometry_renderer;
class tex_quad_renderer;
class timer;
class window;

class rendering_engine
{
private:
	// Direct3D Variables
	std::shared_ptr<direct3d>							_d3d;
	std::shared_ptr<IDirect3DDevice9>					_d3d_device;
	std::shared_ptr<ID3DXSprite>						_d3d_sprite;
	std::shared_ptr<ID3DXLine>							_d3d_line;

	// Timer Variables
	std::shared_ptr<timer>								_timer;
	std::vector<wchar_t>								_timer_text;
	std::shared_ptr<ID3DXFont>							_timer_font;

	// Camera Variables
	std::shared_ptr<camera>								_camera;
	std::array<D3DXMATRIX, 2>							_projection_matrices;
	std::array<D3DXMATRIX, 2>::iterator					_current_projection_matrix;

	// Object Rendering Variables
	std::shared_ptr<tex_quad_renderer>					_tqr;

	std::shared_ptr<b2d_debug_draw>						_b2d_debug_draw;

	std::wstring										_path_to_background_image;
	std::shared_ptr<IDirect3DTexture9>					_background_image;

	std::shared_ptr<IDirect3DTexture9>					_shadow_map;

	std::shared_ptr<shadow_geometry_renderer>			_sgr;

	std::vector<game_object*>							_objects_to_render;
	std::vector<point_light>								_point_lights;

	/** Create the font that will be used to render the timer text. */
	void _create_timer_font();
	/** Render the timer text. */
	void _render_timer();

public:
	rendering_engine(std::shared_ptr<window> window, std::shared_ptr<camera> camera);

	/** Set the timer that the RenderingEngine should render text for. */
	void set_timer_to_render(std::shared_ptr<timer> timer);

	/** Get the debug draw object for box2d. */
	std::shared_ptr<b2d_debug_draw> get_b2d_debug_draw();

	/** @brief Start the rendering engine.
	*  @details No rendering can be done until this function is called. It
	*  should only be called once. */
	void start(std::shared_ptr<box2d> b2d);

	/** Render a scene based on the supplied camera and renderable objects. */
	void render_scene(std::shared_ptr<box2d> b2d);

	/** Returns the IDirect3DDevice9*. */
	std::shared_ptr<IDirect3DDevice9> get_d3ddevice();

	void add_game_object(game_object* go);
	void remove_game_object(game_object* go);

	void render_point_light(const point_light& light);
};