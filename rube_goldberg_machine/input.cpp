#include "input.hpp"

#include "window.hpp"

void input::_current_to_previous()
{
	_key_state_prev = _key_state_curr;
	_mouse_position_prev = _mouse_position_curr;
}

void input::_poll_keyboard()
{
	GetKeyboardState(_key_state_curr.data());
}

void input::_poll_mouse()
{
	GetCursorPos(&_mouse_position_curr);
	// GetCursorPos returns the position of the mouse on the screen, but
	// we want to position of the mouse relative to the client.
	ScreenToClient(_window->get_hwnd(), &_mouse_position_curr);

	_mouse_position_delta.x = _mouse_position_curr.x - _mouse_position_prev.x;
    _mouse_position_delta.y = _mouse_position_curr.y - _mouse_position_prev.y;

	if(_mouse_position_locked)
	{
		// If the mouse is locked, keep the cursor locked to the center of the screen.
		POINT center_of_client = {_window->get_client_width() / 2, _window->get_client_height() / 2};
		_mouse_position_curr = center_of_client;
		ClientToScreen(_window->get_hwnd(), &center_of_client);
		SetCursorPos(center_of_client.x, center_of_client.y);
	}
}

input::input(std::shared_ptr<window> window)
	: _mouse_position_locked(false),
	  _window(window)
{
	ZeroMemory(_key_state_curr.data(), sizeof(keys::value_type)*_key_state_curr.size());
	ZeroMemory(_key_state_prev.data(), sizeof(keys::value_type)*_key_state_prev.size());

	GetCursorPos(&_mouse_position_curr);
	_mouse_position_prev = _mouse_position_curr;
	ZeroMemory(&_mouse_position_delta, sizeof(POINT));
}

bool input::is_key_up(const int key) const
{
	return !is_key_down(key);
}

bool input::is_key_down(const int key) const
{
	return !!(_key_state_curr[key] & 0x80);
}

bool input::is_key_pressed(const int key) const
{
	return !!(((~_key_state_prev[key]) &_key_state_curr[key]) & 0x80);
}

bool input::is_key_released(const int key) const
{
	return !!((_key_state_prev[key] & (~_key_state_curr[key])) & 0x80);
}

POINT input::get_mouse_position() const
{
	return _mouse_position_curr;
}

POINT input::get_mouse_position_delta() const
{
	return _mouse_position_delta;
}

void input::lock_mouse()
{
	if(!_mouse_position_locked)
		ShowCursor(false);

	_mouse_position_locked = true;
}

void input::unlock_mouse()
{
	if(_mouse_position_locked)
		ShowCursor(true);

	_mouse_position_locked = false;
}

bool input::is_mouse_locked() const
{
	return _mouse_position_locked;
}

void input::poll_input_devices()
{
	// _current_to_previous should be called before polling devices each frame.
	_current_to_previous();

	_poll_keyboard();
	_poll_mouse();
}