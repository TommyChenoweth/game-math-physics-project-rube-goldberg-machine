#pragma once

#include "game_object.hpp"

class peg : public game_object
{
private:
	convex_hull _hull;
	b2Body*	_physics_body;
	D3DXVECTOR3 _position;
	float _radius;

	virtual hull_type _do_get_hull_type() const;
	virtual convex_hull _do_get_convex_hull() const;
	virtual circle_hull _do_get_circle_hull() const;

	virtual b2Body* _do_get_physics_body();

	virtual D3DXVECTOR3 _do_get_position() const;
	virtual const RECT* _do_get_render_rect() const;

public:
	peg(const D3DXVECTOR3& position, b2World* world, IDirect3DDevice9* d3d_device);

};