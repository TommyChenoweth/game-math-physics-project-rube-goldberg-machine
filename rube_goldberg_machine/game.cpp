#include "game.hpp"

#include "ball_game_object.hpp"
#include "box2d.hpp"
#include "button.hpp"
#include "camera.hpp"
#include "domino.hpp"
#include "input.hpp"
#include "peg.hpp"
#include "platform.hpp"
#include "rendering_engine.hpp"
#include "timer.hpp"
#include "window.hpp"

#include <iostream>

game* g_game = nullptr;

void game::_set_custom_wnd_proc()
{
	auto custom_wnd_proc =
		[this](HWND window_handle, UINT message, WPARAM w_param, LPARAM l_param) -> LRESULT
	{
		switch(message)
		{
			case WM_DESTROY:
				PostQuitMessage(0);
				break;

			case WM_MOVING:
				if(_timer && !_timer->is_paused())
					_timer->pause();
				break;

			case WM_EXITSIZEMOVE:
				if(_timer && _timer->is_paused())
					_timer->resume();
				break;

			default:
				return DefWindowProc(window_handle, message, w_param, l_param);
		}

		return 0;
	};

	_window->set_wnd_proc(custom_wnd_proc);
}

void game::_build_game_objects()
{
	auto ppm = _b2d->get_pixels_per_meter();
	auto mpp = _b2d->get_meters_per_pixel();

	_top_left_light.pos_and_radius = mpp*D3DXVECTOR4(15.0f, 550.0f, 0.0f, 2000.0f);

	_start_vertical.reset(new platform(D3DXVECTOR3(mpp*400.0f, mpp*545.0f, -1.0f), _b2d->get_world().get(), ppm, mpp*32.0f, mpp*55.0f, _rendering_engine->get_d3ddevice().get()));
	_rendering_engine->add_game_object(_start_vertical.get());

	_start_horizontal.reset(new platform(D3DXVECTOR3(mpp*82.0f, mpp*493.0f, -1.0f), _b2d->get_world().get(), ppm, mpp*350.0f, mpp*32.0f, _rendering_engine->get_d3ddevice().get()));
	_rendering_engine->add_game_object(_start_horizontal.get());

	auto sh_body = _start_horizontal->get_physics_body();
	auto sh_pos = sh_body->GetPosition();
	sh_body->SetTransform(sh_pos, D3DX_PI*0.10f);

	_start_catch_vertical.reset(new platform(D3DXVECTOR3(mpp*82.0f, mpp*418.0f, -1.0f), _b2d->get_world().get(), ppm, mpp*32.0f, mpp*63.0f, _rendering_engine->get_d3ddevice().get()));
	_rendering_engine->add_game_object(_start_catch_vertical.get());

	_start_catch_horizontal.reset(new platform(D3DXVECTOR3(mpp*0.0f, mpp*418.0f, -1.0f), _b2d->get_world().get(), ppm, mpp*114.0f, mpp*32.0f, _rendering_engine->get_d3ddevice().get()));
	_rendering_engine->add_game_object(_start_catch_horizontal.get());

	_first_ball.reset(new ball_game_object(L"../textures/ball_dark_blue.dds", false, D3DXVECTOR3(mpp*300.0f, mpp*550.0f, -1.0f), mpp*16.0f, _b2d->get_world().get(), _rendering_engine->get_d3ddevice().get()));
	_rendering_engine->add_game_object(_first_ball.get());

	_first_button.reset(new button(mpp*D3DXVECTOR3(16.0f, 450.0f, -1.0f), _b2d->get_world().get(), _rendering_engine->get_d3ddevice().get()));
	_rendering_engine->add_game_object(_first_button.get());

	_second_button.reset(new button(mpp*D3DXVECTOR3(600.0f, 5.0f, -1.0f), _b2d->get_world().get(), _rendering_engine->get_d3ddevice().get()));
	_rendering_engine->add_game_object(_second_button.get());

	_final_button.reset(new button(mpp*D3DXVECTOR3(16.0f, 5.0f, -1.0f), _b2d->get_world().get(), _rendering_engine->get_d3ddevice().get()));
	_rendering_engine->add_game_object(_final_button.get());

	_final_slide_01.reset(new platform(mpp*D3DXVECTOR3(0.0f, 300.0f, -1.0f), _b2d->get_world().get(), ppm, mpp*120.0f, mpp*32.0f, _rendering_engine->get_d3ddevice().get()));
	_rendering_engine->add_game_object(_final_slide_01.get());
	auto body = _final_slide_01->get_physics_body();
	auto body_pos = body->GetPosition();
	body->SetTransform(body_pos, D3DX_PI / 180.0f * -37.0f);

	_final_slide_02.reset(new platform(mpp*D3DXVECTOR3(130.0f, 250.0f, -1.0f), _b2d->get_world().get(), ppm, mpp*120.0f, mpp*32.0f, _rendering_engine->get_d3ddevice().get()));
	_rendering_engine->add_game_object(_final_slide_02.get());
	body = _final_slide_02->get_physics_body();
	body_pos = body->GetPosition();
	body->SetTransform(body_pos, D3DX_PI / 180.0f * 37.0f);

	_final_slide_03.reset(new platform(mpp*D3DXVECTOR3(0.0f, 150.0f, -1.0f), _b2d->get_world().get(), ppm, mpp*240.0f, mpp*32.0f, _rendering_engine->get_d3ddevice().get()));
	_rendering_engine->add_game_object(_final_slide_03.get());
	body = _final_slide_03->get_physics_body();
	body_pos = body->GetPosition();
	body->SetTransform(body_pos, D3DX_PI / 180.0f * -37.0f);

	_final_slide_04.reset(new platform(mpp*D3DXVECTOR3(240.0f, 75.0f, -1.0f), _b2d->get_world().get(), ppm, mpp*120.0f, mpp*32.0f, _rendering_engine->get_d3ddevice().get()));
	_rendering_engine->add_game_object(_final_slide_04.get());
	body = _final_slide_04->get_physics_body();
	body_pos = body->GetPosition();
	body->SetTransform(body_pos, D3DX_PI / 180.0f * 37.0f);

	_place_pegs();
	_place_dominos();
}

void game::_place_pegs()
{
	auto ppm = _b2d->get_pixels_per_meter();
	auto mpp = _b2d->get_meters_per_pixel();

	int resolution = 7;
	for(int i = 0; i < resolution; ++i)
	{
		const float step = 25.0f;
		const float right(650.0f + i*step);
		const float gap(120.0f);
		const float top(540.0f);
		const float bottom(370.0f);

		auto p = std::shared_ptr<peg>(new peg(mpp*D3DXVECTOR3(right, i * ((top - bottom) / resolution) + bottom, 0.0f), _b2d->get_world().get(), _rendering_engine->get_d3ddevice().get()));
		auto q = std::shared_ptr<peg>(new peg(mpp*D3DXVECTOR3(right - gap, i * ((top - bottom) / resolution) + bottom, 0.0f), _b2d->get_world().get(), _rendering_engine->get_d3ddevice().get()));

		_pegs.push_back(p);
		_pegs.push_back(q);

		_rendering_engine->add_game_object(p.get());
		_rendering_engine->add_game_object(q.get());
	}

	for(int i = 0; i < resolution; ++i)
	{
		const float step = 25.0f;
		const float left(660.0f - i*step);
		const float gap(120.0f);
		const float top(370.0f);
		const float bottom(200.0f);

		auto p = std::shared_ptr<peg>(new peg(mpp*D3DXVECTOR3(left, i * ((top - bottom) / resolution) + bottom, 0.0f), _b2d->get_world().get(), _rendering_engine->get_d3ddevice().get()));
		auto q = std::shared_ptr<peg>(new peg(mpp*D3DXVECTOR3(left + gap, i * ((top - bottom) / resolution) + bottom, 0.0f), _b2d->get_world().get(), _rendering_engine->get_d3ddevice().get()));

		_pegs.push_back(p);
		_pegs.push_back(q);

		_rendering_engine->add_game_object(p.get());
		_rendering_engine->add_game_object(q.get());
	}

	for(int i = 0; i < resolution; ++i)
	{
		const float step = 25.0f;
		//const float left(660.0f - i*step);
		const float right(650.0f + i*step);
		const float gap(120.0f);
		const float top(200.0f);
		const float bottom(0.0f);

		auto p = std::shared_ptr<peg>(new peg(mpp*D3DXVECTOR3(right, i * ((top - bottom) / resolution) + bottom, 0.0f), _b2d->get_world().get(), _rendering_engine->get_d3ddevice().get()));
		auto q = std::shared_ptr<peg>(new peg(mpp*D3DXVECTOR3(right - gap, i * ((top - bottom) / resolution) + bottom, 0.0f), _b2d->get_world().get(), _rendering_engine->get_d3ddevice().get()));

		_pegs.push_back(p);
		_pegs.push_back(q);

		_rendering_engine->add_game_object(p.get());
		_rendering_engine->add_game_object(q.get());
	}
}

void game::_place_dominos()
{
	auto ppm = _b2d->get_pixels_per_meter();
	auto mpp = _b2d->get_meters_per_pixel();

	auto world = _b2d->get_world().get();

	for(unsigned i = 0; i < _dominos.size(); ++i)
	{
		auto& current_domino = _dominos[i];

		// Destroy any existing dominos.
		if(current_domino)
		{
			_rendering_engine->remove_game_object(current_domino.get());
			auto b = current_domino->get_physics_body();
			world->DestroyBody(b);
		}

		// Place new dominos.
		current_domino.reset(new domino(mpp*D3DXVECTOR3(180.0f - 20.0f*i, 5.0f, -1.0f), _b2d->get_world().get(), ppm, mpp*8.0f, mpp*45.0f, _rendering_engine->get_d3ddevice().get()));
		_rendering_engine->add_game_object(current_domino.get());
	}
}

void game::_stage_two()
{
	auto ppm = _b2d->get_pixels_per_meter();
	auto mpp = _b2d->get_meters_per_pixel();

	point_light ball_light = {mpp*D3DXVECTOR4(0.0f, 0.0f, 0.0f, 750.0f)};

	_second_ball.reset(new ball_game_object(L"../textures/ball_glowing_green_02.dds", true, D3DXVECTOR3(mpp*740.0f, mpp*550.0f, -1.0f), mpp*16.0f, _b2d->get_world().get(), _rendering_engine->get_d3ddevice().get()));
	point_light sb_light = {D3DXVECTOR4(0.0f, 0.0f, 0.0f, 21.0f)};
	_second_ball->attach_point_light(ball_light);
	_rendering_engine->add_game_object(_second_ball.get());
}

void game::_stage_three()
{
	auto ppm = _b2d->get_pixels_per_meter();
	auto mpp = _b2d->get_meters_per_pixel();

	point_light ball_light = {mpp*D3DXVECTOR4(0.0f, 0.0f, 0.0f, 750.0f)};

	_third_ball.reset(new ball_game_object(L"../textures/ball_glowing_blue.dds", true, D3DXVECTOR3(mpp*50.0f, mpp*400.0f, -1.0f), mpp*16.0f, _b2d->get_world().get(), _rendering_engine->get_d3ddevice().get()));
	point_light sb_light = {D3DXVECTOR4(0.0f, 0.0f, 0.0f, 21.0f)};
	_third_ball->get_physics_body()->ApplyForceToCenter(b2Vec2(0.0f, -1.0f), true);
	_third_ball->attach_point_light(ball_light);
	_rendering_engine->add_game_object(_third_ball.get());
}

void game::_reset_stage()
{
	auto ppm = _b2d->get_pixels_per_meter();
	auto mpp = _b2d->get_meters_per_pixel();

	auto world = _b2d->get_world().get();

	_sound_engine->stopAllSounds();

	auto first_ball_body = _first_ball->get_physics_body();
	world->DestroyBody(first_ball_body);
	_rendering_engine->remove_game_object(_first_ball.get());
	_first_ball.reset();

	_first_ball.reset(new ball_game_object(L"../textures/ball_dark_blue.dds", false, D3DXVECTOR3(mpp*300.0f, mpp*550.0f, -1.0f), mpp*16.0f, _b2d->get_world().get(), _rendering_engine->get_d3ddevice().get()));
	_rendering_engine->add_game_object(_first_ball.get());

	if(_second_ball)
	{
		auto second_ball_body = _second_ball->get_physics_body();
		world->DestroyBody(second_ball_body);
		_rendering_engine->remove_game_object(_second_ball.get());
		_second_ball.reset();
	}

	if(_third_ball)
	{
		auto third_ball_body = _third_ball->get_physics_body();
		world->DestroyBody(third_ball_body);
		_rendering_engine->remove_game_object(_third_ball.get());
		_third_ball.reset();
	}

	_place_dominos();

	_stage_two_started = false;
	_stage_two_init = false;
	_stage_three_started = false;
	_stage_three_init = false;
	_final_stage_init = false;
}

game::game(HINSTANCE instance_handle)
	: _world_width_meters(100.0f),
	_world_height_meters(100.0f),
	_window(new window(instance_handle, L"Rube Goldberg Machine", 800, 600)),
	_camera(new camera(D3DXVECTOR3(400.0f / 64.0f, 300.0f / 64.0f, -100.0f))),
	_b2d(new box2d(64.0f, _world_width_meters, _world_height_meters)),
	_input(new input(_window)),
	_timer(new timer()),
	_rendering_engine(new rendering_engine(_window, _camera)),
	_stage_two_started(false),
	_stage_two_init(false),
	_stage_three_started(false),
	_stage_three_init(false),
	_final_stage_init(false),
	_sound_engine(nullptr)
{
	g_game = this;

	// Display the main window.
	_window->show();
	_set_custom_wnd_proc();

	_rendering_engine->set_timer_to_render(_timer);
	_rendering_engine->start(_b2d);

	_build_game_objects();

	_sound_engine = irrklang::createIrrKlangDevice();
}

void game::run()
{
	MSG msg = {0};
    while(WM_QUIT != msg.message)
    {
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else
        {
			_timer->update();
			float dt = _timer->get_dt();

			_input->poll_input_devices();

			// Print the cursor position when the left mouse button is pressed.
			if(_input->is_key_pressed(VK_LBUTTON))
			{
				auto mouse_position = _input->get_mouse_position();
				mouse_position.y = _window->get_client_height() - mouse_position.y;
				std::cout << mouse_position.x << ", " << mouse_position.y << std::endl;
			}

			if(_input->is_key_pressed(VK_BACK))
			{
				_reset_stage();
			}

			if(_input->is_key_pressed(VK_ESCAPE))
			{
				PostQuitMessage(0);
			}

			_b2d->step();

			_rendering_engine->render_point_light(_top_left_light);

			auto b1_body = _first_button->get_physics_body();
			auto contact_list = b1_body->GetContactList();
			if(contact_list && contact_list->contact->IsTouching())
			{
				_stage_two_started = true;
			}

			if(_stage_two_started)
			{
				if(!_stage_two_init)
				{
					_stage_two();
					_stage_two_init = true;
					_sound_engine->play2D("../sounds/into_the_light_clip.mp3", false);
				}

				auto pl = _second_ball->get_point_light();
				_rendering_engine->render_point_light(pl);
			}

			auto b2_body = _second_button->get_physics_body();
			contact_list = b2_body->GetContactList();
			if(contact_list && contact_list->contact->IsTouching())
			{
				_stage_three_started = true;
			}

			if(_stage_three_started)
			{
				if(!_stage_three_init)
				{
					_stage_three();
					_stage_three_init = true;
				}

				auto pl = _third_ball->get_point_light();
				_rendering_engine->render_point_light(pl);
			}

			_rendering_engine->render_scene(_b2d);

			auto b3_body = _final_button->get_physics_body();
			contact_list = b3_body->GetContactList();
			if(contact_list && contact_list->contact->IsTouching())
			{
				if(!_final_stage_init)
				{
					_sound_engine->play2D("../sounds/yay.wav", false);
					_final_stage_init = true;
				}
			}
        }
    }
}

timer* game::get_timer()
{
	return _timer.get();
}