#pragma once

#include <array>
#include "point_light.hpp"
#include "tqr_instance.hpp"
#include "tqr_vertex.hpp"
#include <d3dx9.h>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

/**
 *	Renders textured quads at world coordinates facing down the -z axis using
 *  the programmable pipeline.
 *
 *  To use this class, call render_texture and pass in a texture for each quad that you
 *  want drawn. When you're finished adding textures to draw, call draw. Note that draw
 *  should only need to be called once per frame.
 *
 *  To reduce draw calls, quads sharing the same texture are drawn in batches.
 *  Vertex buffers are dynamic to reduce the penalty for writing to them.
 *  Read more about that here: 
 *  http://msdn.microsoft.com/en-us/library/windows/desktop/bb147263%28v=vs.85%29.aspx
 *  Instancing is supported, but is currently unnecessary.
 */
class tex_quad_renderer
{
private:

	struct textured_quad
	{
		D3DXVECTOR3	position;
		RECT		source_rect;
		D3DXMATRIX  transform;
	};

	struct textured_quad_batch
	{
		IDirect3DTexture9*						texture;
		float									texture_width;
		float									texture_height;

		// We're using a vector to maximise spatial locality.
		std::vector<textured_quad> quads;
	};

	typedef std::unordered_map<IDirect3DTexture9*, textured_quad_batch> batched_quads;

	// Batch Variables
	batched_quads	_batched_quads;
	unsigned		_number_of_quads_batched;

	// Vertex/Instance/Index Buffer Variables
	std::shared_ptr<IDirect3DDevice9>			_d3d_device;
	std::shared_ptr<IDirect3DVertexBuffer9>		_vertex_buffer;
	std::shared_ptr<IDirect3DVertexBuffer9>		_instance_data;
	std::shared_ptr<IDirect3DIndexBuffer9>		_index_buffer;
	unsigned									_vertex_buffer_index;
	static const unsigned						_QUAD_CAPACITY = 500;
	static const unsigned						_VERTICES_PER_QUAD = 4;
	static const unsigned						_INDICES_PER_QUAD = 6;
	static const unsigned						_TRIS_PER_QUAD = 2;

	const float		_pixels_per_meter;
	const float		_meters_per_pixel;

	// Shader Variables
	std::shared_ptr<ID3DXEffect>	_effect;
	std::wstring					_effect_file_path;
	D3DXHANDLE						_effect_handle;
	D3DXHANDLE						_effect_vp_handle;
	D3DXHANDLE						_effect_texture_handle;
	D3DXHANDLE						_effect_texture_width;
	D3DXHANDLE						_effect_texture_height;
	D3DXHANDLE						_effect_ambient_light;
	D3DXHANDLE						_effect_point_light_01;
	D3DXHANDLE						_effect_point_light_02;
	D3DXHANDLE						_effect_point_light_03;
	D3DXHANDLE						_effect_point_lights;
	D3DXHANDLE						_effect_shadow_map;

	// Lighting
	D3DXCOLOR					_ambient_light;
	unsigned					_point_lights_count;
	std::array<point_light, 10>	_point_lights;

	void _create_vertex_buffer();
	void _create_instance_buffer();
	void _create_index_buffer();

	void _create_effect();
	void _load_effect_handles();

	void _create_vertices_for_quads();
	void _create_indices_for_quads();

public:
	tex_quad_renderer(std::shared_ptr<IDirect3DDevice9> d3d_device, const float pixels_per_meter);

	D3DXCOLOR get_ambient_light() const;
	void set_ambient_light(const D3DXCOLOR& ambient_light);

	void set_point_light(const point_light& point_light);
	void clear_point_lights();

	void render_texture(IDirect3DTexture9* texture, const RECT* source_rect, const D3DXMATRIX& transform, const D3DXVECTOR3& position);
	void draw(const D3DXMATRIX& view_projection, IDirect3DTexture9* shadow_map);
};