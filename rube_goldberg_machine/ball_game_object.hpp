#pragma once

#include "game_object.hpp"

#include "point_light.hpp"

#include <Box2D\Box2D.h>

class ball_game_object : public game_object
{
private:
	convex_hull _hull;
	b2Body*	_physics_body;
	D3DXVECTOR3 _position;
	float		_radius;
	point_light	_light;

	virtual hull_type _do_get_hull_type() const;
	virtual convex_hull _do_get_convex_hull() const;
	virtual circle_hull _do_get_circle_hull() const;

	virtual b2Body* _do_get_physics_body();

	virtual D3DXVECTOR3 _do_get_position() const;
	virtual const RECT* _do_get_render_rect() const;

public:
	ball_game_object(const std::wstring& path_to_texture_file, bool glows, const D3DXVECTOR3& position, const float radius, b2World* world, IDirect3DDevice9* d3d_device);

	void attach_point_light(const point_light& light);
	point_light get_point_light();
};