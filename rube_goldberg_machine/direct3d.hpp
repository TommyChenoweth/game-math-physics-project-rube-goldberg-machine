#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#include <memory>
#include <Windows.h>

class window;

class direct3d
{
private:
	std::shared_ptr<IDirect3D9>			_d3d_object;
	std::shared_ptr<IDirect3DDevice9>	_d3d_device;
	std::shared_ptr<ID3DXSprite>		_d3d_sprite;
	UINT								_adapter;
	D3DPRESENT_PARAMETERS				_present_params;
	DWORD								_requested_vertex_processing;
	DWORD								_device_behavior_flags;
	D3DDEVTYPE							_device_type;
	D3DFORMAT							_adapter_format;
	D3DFORMAT							_backbuffer_format;
	float								_backbuffer_width;
	float								_backbuffer_height;
	UINT								_refresh_rate;

	float								_vertical_fov_degrees;
	float								_aspect_ratio;

	std::shared_ptr<window>				_window;

	// Functions for initializing Direct3D
	HRESULT _create_d3d_object();
	HRESULT _verify_hal_and_format_support();
	HRESULT _verify_hardware_vertex_processing();
	bool	_shaders_are_supported();
	void	_set_present_params();
	HRESULT _create_device_interface();
	void	_create_sprite_object();

public:
	/** @param window The window in which Direct3D will run.
	 */
	direct3d(std::shared_ptr<window> window);

	/** @brief Initializes Direct3D.
	 */
	void init_d3d();

	bool started() const;

	std::shared_ptr<IDirect3D9> get_d3d_object();
	std::shared_ptr<IDirect3DDevice9> get_d3d_device();
	std::shared_ptr<ID3DXSprite> get_d3d_sprite();

	float get_backbuffer_width() const;
	float get_back_buffer_height() const;
	float get_vertical_fov() const;
	float get_aspect_ratio() const;
	UINT get_refresh_rate_hz() const;
};