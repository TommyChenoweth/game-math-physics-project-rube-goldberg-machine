#pragma once

#include <functional>
#include <string>
#include <Windows.h>

/** @brief Simplifies the creation and display of windows.
 */
class window
{
private:
	HINSTANCE	_instance_handle;
	HWND		_window_handle;

	WNDCLASSEX	 _class;
	std::wstring _class_name;
	std::wstring _icon_path;

	std::wstring _window_title;

	unsigned _client_width;
	unsigned _client_height;

	HRESULT _register_class();
	HRESULT _create_window();

	std::function<LRESULT(HWND, UINT, WPARAM, LPARAM)> _user_wnd_proc;

public:
	/** Note that the window's dimensions are determined by - but not equal to - the
	 *  the \paramname{client_width} and \paramname{client_height} parameters.
	 *  @param instance_handle The instance handle that was passed into winmain.
	 *  @param title The text that will appear in the window's title bar.
	 *  @param client_width The desired width of the client area of the window.
	 *  @param client_height The desired height of the client area of the window.
	 */
	window(HINSTANCE instance_handle, const std::wstring& title, unsigned client_width, unsigned client_height);

	/** Makes the window visible.
	 */
	void show();

	/** @brief Handles the messages passed to the window. 
	 *  The messages are forwarded to this function via MainWndProc() in window.cpp.
	 *  For parameter documention, see 
	 *  <a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ms633573%28v=vs.85%29.aspx">this article</a> on MSDN.
	 */
	LRESULT wnd_proc(UINT message, WPARAM w_param, LPARAM l_param);

	/** @brief Forwards windows messages to the passed callable object.
	 *  The passed object is responsible for handling _all_ windows messages; the
	 *  window class will cease to handle them on its own.
	 *  @param wnd_proc The callable object to which all windows messages will be forwarded.
	 */
	void set_wnd_proc(std::function<LRESULT(HWND, UINT, WPARAM, LPARAM)> wnd_proc);

	HWND get_hwnd();
	unsigned get_client_width() const;
	unsigned get_client_height() const;
};