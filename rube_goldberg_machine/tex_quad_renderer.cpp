#include "tex_quad_renderer.hpp"

#include "com_pointer.hpp"
#include "debug.hpp"

#include <algorithm>
#include <iostream>

void tex_quad_renderer::_create_vertex_buffer()
{
	IDirect3DVertexBuffer9* vertex_buffer(nullptr);
	HR(_d3d_device->CreateVertexBuffer(
		_QUAD_CAPACITY * _VERTICES_PER_QUAD * sizeof(tqr_vertex),
		D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY, 
		0, 
		D3DPOOL_DEFAULT, 
		&vertex_buffer,
		nullptr);
	_vertex_buffer = create_com_pointer(vertex_buffer));
}

void tex_quad_renderer::_create_instance_buffer()
{
	IDirect3DVertexBuffer9* instance_data(nullptr);
	HR(_d3d_device->CreateVertexBuffer(
		_QUAD_CAPACITY * sizeof(tqr_instance),
		D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
		0,
		D3DPOOL_DEFAULT,
		&instance_data,
		nullptr));
	_instance_data = create_com_pointer(instance_data);
}

void tex_quad_renderer::_create_index_buffer()
{
	IDirect3DIndexBuffer9* index_buffer(nullptr);
	HR(_d3d_device->CreateIndexBuffer(
		_QUAD_CAPACITY * _INDICES_PER_QUAD * sizeof(unsigned __int16), 
		0,
		D3DFMT_INDEX16, 
		D3DPOOL_DEFAULT, 
		&index_buffer, 
		nullptr));
	_index_buffer = create_com_pointer(index_buffer);
}

/**
 * Create an effect from the .fx file.
 */
void tex_quad_renderer::_create_effect()
{
	ID3DXEffect* effect(nullptr);
	ID3DXBuffer* error_buffer(nullptr);

	DWORD shader_flags = 0;
	#ifdef _DEBUG
	shader_flags |= D3DXSHADER_DEBUG | D3DXSHADER_SKIPOPTIMIZATION;
	#endif

	auto r = D3DXCreateEffectFromFile(
		_d3d_device.get(),
		_effect_file_path.c_str(),
		nullptr,
		nullptr,
		shader_flags,
		nullptr,
		&effect,
		&error_buffer);
	_effect = create_com_pointer(effect);

	if(error_buffer)
	{
		std::cerr << "SHADER ERRORS: " << reinterpret_cast<char*>(error_buffer->GetBufferPointer()) << std::endl;
		error_buffer->Release();
	}

	HR(r);
}

/**
 *	Load the effect handles from the created effect.
 */
void tex_quad_renderer::_load_effect_handles()
{
	_effect_handle			= _effect->GetTechniqueByName("TQRenderTech");
	_effect_vp_handle		= _effect->GetParameterByName(0, "g_view_projection");
	_effect_texture_handle	= _effect->GetParameterByName(0, "g_texture");
	_effect_texture_width	= _effect->GetParameterByName(0, "g_texture_width");
	_effect_texture_height	= _effect->GetParameterByName(0, "g_texture_height");
	_effect_ambient_light	= _effect->GetParameterByName(0, "g_ambient_light");
	_effect_point_light_01	= _effect->GetParameterByName(0, "g_point_light_01");
	_effect_point_light_02 = _effect->GetParameterByName(0, "g_point_light_02");
	_effect_point_light_03 = _effect->GetParameterByName(0, "g_point_light_03");
	_effect_point_lights	= _effect->GetParameterByName(0, "g_point_lights");
	_effect_shadow_map		= _effect->GetParameterByName(0, "g_shadow_map");
}

/**
 *	Create vertices for each of the textures waiting to be rendered.
 */
void tex_quad_renderer::_create_vertices_for_quads()
{
	DWORD lock_flags(D3DLOCK_NOOVERWRITE);

	bool enough_space_in_buffer
		= (_QUAD_CAPACITY * _VERTICES_PER_QUAD  - _vertex_buffer_index < _number_of_quads_batched * _VERTICES_PER_QUAD);

	if(!enough_space_in_buffer)
	{
		lock_flags = D3DLOCK_DISCARD;
		_vertex_buffer_index = 0;
	}

	tqr_vertex* vertex(nullptr);
	HR(_vertex_buffer->Lock(
		_vertex_buffer_index, 
		_number_of_quads_batched * _VERTICES_PER_QUAD * sizeof(tqr_vertex),
		reinterpret_cast<void**>(&vertex), 
		lock_flags));

	tqr_instance* instance(nullptr);
	HR(_instance_data->Lock(
		_vertex_buffer_index / _VERTICES_PER_QUAD,
		_number_of_quads_batched * sizeof(tqr_instance),
		reinterpret_cast<void**>(&instance),
		lock_flags));

	const float meters_per_pixel = _meters_per_pixel;

	std::for_each(_batched_quads.begin(), _batched_quads.end(),
		[&vertex, &instance, &meters_per_pixel](const batched_quads::value_type& v)
	{
		// Sort the quads from front to back. This will need to be reversed if
		// we want to use alpha blending.
		auto batch = v.second;
		std::sort(batch.quads.begin(), batch.quads.end(), 
			[](const textured_quad& lhs, const textured_quad& rhs)
		{
			return lhs.position.z < rhs.position.z;
		});

		// Build the vertex and instance buffers for the batched quads.
		auto quad_it = batch.quads.begin();
		while(quad_it != batch.quads.end())
		{
			*instance = tqr_instance(quad_it->position, D3DXVECTOR4(0.0f, 0.0f, 1.0f, 1.0f));

			static tqr_vertex quad_verts[_VERTICES_PER_QUAD] =
			{
				/*tqr_vertex(D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR2(0.0f, 1.0f)),
				tqr_vertex(D3DXVECTOR3(0.0f, 1.0f, 0.0f), D3DXVECTOR2(0.0f, 0.0f)),
				tqr_vertex(D3DXVECTOR3(1.0f, 1.0f, 0.0f), D3DXVECTOR2(1.0f, 0.0f)),
				tqr_vertex(D3DXVECTOR3(1.0f, 0.0f, 0.0f), D3DXVECTOR2(1.0f, 1.0f))*/
				tqr_vertex(D3DXVECTOR3(-0.5f, -0.5f, 0.0f), D3DXVECTOR2(0.0f, 1.0f)),
				tqr_vertex(D3DXVECTOR3(-0.5f, 0.5f, 0.0f), D3DXVECTOR2(0.0f, 0.0f)),
				tqr_vertex(D3DXVECTOR3(0.5f, 0.5f, 0.0f), D3DXVECTOR2(1.0f, 0.0f)),
				tqr_vertex(D3DXVECTOR3(0.5f, -0.5f, 0.0f), D3DXVECTOR2(1.0f, 1.0f))
			};

			const float quad_width = static_cast<float>(quad_it->source_rect.right - quad_it->source_rect.left);
			const float quad_height = static_cast<float>(quad_it->source_rect.bottom - quad_it->source_rect.top);

			// Construct this quad's world matrix.
			static D3DXMATRIX scale;
			D3DXMatrixIdentity(&scale);
			scale._11 = quad_width * meters_per_pixel;
			scale._22 = quad_height * meters_per_pixel;

			static D3DXMATRIX translate;
			D3DXMatrixIdentity(&translate);
			translate._41 = quad_it->position.x;
			translate._42 = quad_it->position.y;
			translate._43 = quad_it->position.z;

			const float uv_width = quad_width / batch.texture_width;
			const float uv_height = quad_height / batch.texture_height;
			
			// Construct a matrix that will transform the default uv coordinates
			// to a position that selects the source_rect.
			static D3DXMATRIX uv_transform;
			D3DXMatrixIdentity(&uv_transform);
			uv_transform._11 = uv_width;
			uv_transform._22 = uv_height;
			uv_transform._41 = static_cast<float>(quad_it->source_rect.left / batch.texture_width);
			uv_transform._42 = static_cast<float>(quad_it->source_rect.top / batch.texture_height);
			
			for(unsigned i = 0; i < _VERTICES_PER_QUAD; ++i)
			{
				D3DXVec3TransformCoord(&vertex[i].position, &quad_verts[i].position, &(scale*quad_it->transform*translate));
				D3DXVec2TransformCoord(&vertex[i].texture_coordinates, &quad_verts[i].texture_coordinates, &uv_transform);
				if(fabs(1 - vertex[i].texture_coordinates.x) < 0.01)
				{
					vertex[i].texture_coordinates.x = 1;
				}
				if(fabs(1 - vertex[i].texture_coordinates.y) < 0.01)
				{
					vertex[i].texture_coordinates.y = 1;
				}
			}

			vertex += _VERTICES_PER_QUAD;
			++instance;
			++quad_it;
		}
	});

	HR(_instance_data->Unlock());

	HR(_vertex_buffer->Unlock());
}

/**
 *  Populate the index buffer with indices. Note that this only needs to be done one time
 *  because the order of the vertices is constant.
 */
void tex_quad_renderer::_create_indices_for_quads()
{
	unsigned __int16* index(nullptr);
	HR(_index_buffer->Lock(
		0,
		0,
		reinterpret_cast<void**>(&index), 
		0));

	for(unsigned i = 0; i < _QUAD_CAPACITY; ++i)
	{
		// This is the first triangle in the quad.
		index[(i*_INDICES_PER_QUAD) + 0] = 0 + i*_VERTICES_PER_QUAD;
		index[(i*_INDICES_PER_QUAD) + 1] = 1 + i*_VERTICES_PER_QUAD;
		index[(i*_INDICES_PER_QUAD) + 2] = 2 + i*_VERTICES_PER_QUAD;

		// This is the second triangle in the quad.
		index[(i*_INDICES_PER_QUAD) + 3] = 0 + i*_VERTICES_PER_QUAD;
		index[(i*_INDICES_PER_QUAD) + 4] = 2 + i*_VERTICES_PER_QUAD;
		index[(i*_INDICES_PER_QUAD) + 5] = 3 + i*_VERTICES_PER_QUAD;
	}

	HR(_index_buffer->Unlock());
}

tex_quad_renderer::tex_quad_renderer(std::shared_ptr<IDirect3DDevice9> d3d_device, const float pixels_per_meter)
	: _number_of_quads_batched(0),
	  _d3d_device(d3d_device),
	  _vertex_buffer_index(0),
	  _pixels_per_meter(pixels_per_meter),
	  _meters_per_pixel(1.0f / static_cast<float>(_pixels_per_meter)),
	  _effect_file_path(L"../shaders/tqrenderer.fx"),
	  _ambient_light(D3DCOLOR_XRGB(0, 0, 0)),
	  _point_lights_count(0)
{
	tqr_vertex::initialize_vertex_declaration(_d3d_device);

	_create_vertex_buffer();
	_create_instance_buffer();
	_create_index_buffer();

	_create_effect();
	_load_effect_handles();

	_create_indices_for_quads();

	clear_point_lights();
}

D3DXCOLOR tex_quad_renderer::get_ambient_light() const
{
	return _ambient_light;
}

void tex_quad_renderer::set_ambient_light(const D3DXCOLOR& ambient_light)
{
	_ambient_light = ambient_light;
}

void tex_quad_renderer::set_point_light(const point_light& point_light)
{
	_point_lights[_point_lights_count++] = point_light;
}

void tex_quad_renderer::clear_point_lights()
{
	_point_lights_count = 0;
	ZeroMemory(&_point_lights[0], _point_lights.max_size() * sizeof(point_light));
}

/**
 *	Pass in a texture to be rendered. Note that it won't actually be rendered
 *	until the draw call.
 */
void tex_quad_renderer::render_texture(IDirect3DTexture9* texture, const RECT* source_rect, const D3DXMATRIX& transform, const D3DXVECTOR3& position)
{
	textured_quad_batch batch = {texture};
	auto insert_result = _batched_quads.insert(batched_quads::value_type(texture, batch));

	bool new_batch_was_inserted(insert_result.second);
	if(new_batch_was_inserted)
	{
        // This reserve size is arbitrary.
		insert_result.first->second.quads.reserve(_QUAD_CAPACITY/2);

		// Set the TexQuadBatch's texture dimensions.
		D3DSURFACE_DESC texture_description;
		texture->GetLevelDesc(0, &texture_description);
		insert_result.first->second.texture_width = static_cast<float>(texture_description.Width);
		insert_result.first->second.texture_height = static_cast<float>(texture_description.Height);
	}

	static RECT default_source_rect = {0, 0, 0, 0};
	if(!source_rect)
	{
		// If no source rect was passed in, create a source rect that selects the entire texture.
		default_source_rect.right = static_cast<long>(insert_result.first->second.texture_width);
		default_source_rect.bottom = static_cast<long>(insert_result.first->second.texture_height);
		source_rect = &default_source_rect;
	}

	textured_quad tq = {position, *source_rect, transform};
	insert_result.first->second.quads.push_back(tq);

	++_number_of_quads_batched;
}

/**
 *	Render all of the textures that were passed into the render_texture function.
 */
void tex_quad_renderer::draw(const D3DXMATRIX& view_projection, IDirect3DTexture9* shadow_map)
{
	_create_vertices_for_quads();

	HR(_d3d_device->SetVertexDeclaration(tqr_vertex::get_vertex_declaration().get()));

	HR(_d3d_device->SetStreamSource(0, _vertex_buffer.get(), _vertex_buffer_index*sizeof(tqr_vertex), sizeof(tqr_vertex)));
	HR(_d3d_device->SetStreamSourceFreq(0, D3DSTREAMSOURCE_INDEXEDDATA | _number_of_quads_batched));
	HR(_d3d_device->SetIndices(_index_buffer.get()));

	HR(_d3d_device->SetStreamSource(1, _instance_data.get(), _vertex_buffer_index / _VERTICES_PER_QUAD*sizeof(tqr_instance), sizeof(tqr_instance)));
	HR(_d3d_device->SetStreamSourceFreq(1, D3DSTREAMSOURCE_INSTANCEDATA | 1));

	HR(_effect->SetTechnique(_effect_handle));

	HR(_effect->SetMatrix(_effect_vp_handle, &view_projection));
	HR(_effect->SetValue(_effect_ambient_light, &_ambient_light, sizeof(D3DXCOLOR)));

	HR(_effect->SetRawValue(_effect_point_light_01, &_point_lights[0], 0, sizeof(point_light)));
	HR(_effect->SetRawValue(_effect_point_light_02, &_point_lights[1], 0, sizeof(point_light)));
	HR(_effect->SetRawValue(_effect_point_light_03, &_point_lights[2], 0, sizeof(point_light)));

	HR(_effect->SetTexture(_effect_shadow_map, shadow_map));

	unsigned vertex_offset = 0;
	for(auto& texture_batch_pair : _batched_quads)
	{
		auto batch = texture_batch_pair.second;
		unsigned quads_in_this_batch(batch.quads.size());

		HR(_effect->SetTexture(_effect_texture_handle, batch.texture));
		HR(_effect->SetFloat(_effect_texture_width, batch.texture_width));
		HR(_effect->SetFloat(_effect_texture_height, batch.texture_height));

		UINT number_of_passes(0);
		HR(_effect->Begin(&number_of_passes, 0));

		for(unsigned current_pass = 0; current_pass < number_of_passes; ++current_pass)
		{
			HR(_effect->BeginPass(current_pass));
			HR(_d3d_device->DrawIndexedPrimitive(
				D3DPT_TRIANGLELIST, 
				vertex_offset, 
				0, 
				quads_in_this_batch * _VERTICES_PER_QUAD, 
				0, 
				quads_in_this_batch * _TRIS_PER_QUAD));
			HR(_effect->EndPass());
		}

		HR(_effect->End());
		vertex_offset += quads_in_this_batch * _VERTICES_PER_QUAD;
	}

	_vertex_buffer_index += vertex_offset;
	_number_of_quads_batched = 0;
	_batched_quads.clear();

	HR(_d3d_device->SetStreamSourceFreq(0, 1));
	HR(_d3d_device->SetStreamSourceFreq(1, 1));
}