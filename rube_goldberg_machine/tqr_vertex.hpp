#pragma once

#include <d3dx9.h>
#include <memory>

/**
 *  Represents a vertex of a Textured Quad.
 */
class tqr_vertex
{
private:
	static std::shared_ptr<IDirect3DVertexDeclaration9>	_vertex_declaration;

public:
	D3DXVECTOR3 position;
	D3DXVECTOR2 texture_coordinates;

	tqr_vertex(const D3DXVECTOR3& position, const D3DXVECTOR2& texture_coordinates);
	tqr_vertex();

	static void initialize_vertex_declaration(std::shared_ptr<IDirect3DDevice9> d3d_device);
	static std::shared_ptr<IDirect3DVertexDeclaration9> get_vertex_declaration();
};