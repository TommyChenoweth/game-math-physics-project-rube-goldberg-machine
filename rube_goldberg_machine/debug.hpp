#pragma once

// It seems as though d3d9.h MUST be included BEFORE DxErr. 
#include <d3d9.h>
#include <DxErr.h>

/**
 *	This is a collection of macros that are useful for debugging.
 */

#ifndef display_error
#define display_error(result)											\
{																		\
	DXTrace(__FILE__, __LINE__, result, __LPREFIX(__FUNCTION__), true);	\
}				
#endif

/**
 *	This macro was created by Frank D. Luna.
 *	http://www.d3dcoder.net/d3d9c.htm
 */
#if defined(_DEBUG)
	#ifndef HR
	#define HR(x)												\
	{															\
		HRESULT hr = x;											\
		if(FAILED(hr))											\
		{														\
			DXTrace(__FILE__, (DWORD)__LINE__, hr, L#x, TRUE);	\
		}														\
	}
	#endif

#else
	#ifndef HR
	#define HR(x) x;
	#endif
#endif 