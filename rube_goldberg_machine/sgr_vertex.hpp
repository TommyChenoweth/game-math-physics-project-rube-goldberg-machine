#pragma once

#include <d3dx9.h>
#include <memory>

class sgr_vertex
{
private:
	static std::shared_ptr<IDirect3DVertexDeclaration9>	_vertex_declaration;

public:
	D3DXVECTOR3 position;
	D3DXVECTOR4 color;

	sgr_vertex(const D3DXVECTOR3& position, const D3DXVECTOR4& color);
	sgr_vertex();

	static void initialize_vertex_declaration(std::shared_ptr<IDirect3DDevice9> d3d_device);
	static std::shared_ptr<IDirect3DVertexDeclaration9> get_vertex_declaration();
};