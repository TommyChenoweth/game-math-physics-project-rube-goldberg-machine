#include "game_contact_listener.hpp"

void game_contact_listener::BeginContact(b2Contact *contact)
{
}

void game_contact_listener::EndContact(b2Contact *contact)
{
}

void game_contact_listener::PreSolve(b2Contact *contact, const b2Manifold *oldManifold)
{
}

void game_contact_listener::PostSolve(b2Contact *contact, const b2ContactImpulse *impulse)
{
}
