#include "game_object.hpp"

#include "com_pointer.hpp"

IDirect3DTexture9* game_object::_create_texture_from_file(const std::wstring& path_to_texure_file, IDirect3DDevice9* d3d_device)
{
	IDirect3DTexture9* texture(nullptr);
	D3DXCreateTextureFromFile(
		d3d_device,
		path_to_texure_file.c_str(),
		&texture);
	return texture;
}

const RECT* game_object::_do_get_render_rect() const
{
	return nullptr;
}

game_object::game_object(const std::wstring& path_to_texure_file, IDirect3DDevice9* d3d_device)
{
	auto texture = _create_texture_from_file(path_to_texure_file, d3d_device);
	_texture = create_com_pointer(texture);
}

game_object::hull_type game_object::get_hull_type() const
{
	return _do_get_hull_type();
}

convex_hull game_object::get_convex_hull() const
{
	return _do_get_convex_hull();
}

circle_hull game_object::get_circle_hull() const
{
	return _do_get_circle_hull();
}

b2Body* game_object::get_physics_body()
{
	return _do_get_physics_body();
}

D3DXVECTOR3 game_object::get_position() const
{
	return _do_get_position();
}

IDirect3DTexture9* game_object::get_texture() const
{
	return _texture.get();
}

const RECT* game_object::get_render_rect() const
{
	return _do_get_render_rect();
}