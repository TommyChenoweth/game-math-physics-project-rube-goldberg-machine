#include "rendering_engine.hpp"

#include "box2d.hpp"
#include "b2d_debug_draw.hpp"
#include "camera.hpp"
#include "com_pointer.hpp"
#include "debug.hpp"
#include "direct3d.hpp"
#include "game_object.hpp"
#include "shadow_geometry_renderer.hpp"
#include "tex_quad_renderer.hpp"
#include "timer.hpp"

#include <strsafe.h>

void rendering_engine::_create_timer_font()
{
	D3DXFONT_DESC font_description;
	font_description.Height = 18;
	font_description.Width = 0;
	font_description.Weight = 0;
	font_description.MipLevels = 1;
	font_description.Italic = false;
	font_description.CharSet = DEFAULT_CHARSET;
	font_description.OutputPrecision = OUT_DEFAULT_PRECIS;
	font_description.Quality = DEFAULT_QUALITY;
	font_description.PitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;

	std::wstring font_name(L"Times New Roman");
	wcsncpy_s(font_description.FaceName, sizeof(font_description.FaceName) / sizeof(WORD), font_name.c_str(), 32);

	ID3DXFont* font(nullptr);
	D3DXCreateFontIndirect(_d3d_device.get(), &font_description, &font);
	_timer_font = create_com_pointer(font);
}

void rendering_engine::_render_timer()
{
	static float seconds_elapsed = 0.0f;
	seconds_elapsed += _timer->get_dt();

	// Update the timer text once per second.
	if(1.0f <= seconds_elapsed)
	{
		seconds_elapsed -= 1.0f;
		float mspf = _timer->get_mspf();
		float fps = _timer->get_fps();
		StringCbPrintf(&_timer_text[0], _timer_text.size(), L"Frames Per Second = %f\nMilliseconds Per Frame = %f", fps, mspf);
		_timer_font->PreloadText(&_timer_text[0], _timer_text.size());
	}

	static RECT r = {5, 5, 0, 0};
	_timer_font->DrawText(_d3d_sprite.get(), &_timer_text[0], -1, &r, DT_NOCLIP, D3DCOLOR_XRGB(0, 0, 0));
}

rendering_engine::rendering_engine(std::shared_ptr<window> window, std::shared_ptr<camera> camera)
:	  _d3d(new direct3d(window)),
	  _d3d_device(nullptr),
	  _d3d_sprite(nullptr),
	  _d3d_line(nullptr),
	  _timer(nullptr),
	  _timer_text(128, 0),
	  _timer_font(nullptr),
	  _camera(camera),
	  _tqr(nullptr),
	  _b2d_debug_draw(nullptr),
	  _path_to_background_image(L"../textures/bg.dds"),
	  _background_image(nullptr),
	  _shadow_map(nullptr),
	  _sgr(nullptr)
{
	D3DXMatrixIdentity(&_projection_matrices[0]);
	D3DXMatrixIdentity(&_projection_matrices[1]);
}

void rendering_engine::set_timer_to_render(std::shared_ptr<timer> timer)
{
	_timer = timer;
}

std::shared_ptr<b2d_debug_draw> rendering_engine::get_b2d_debug_draw()
{
	return _b2d_debug_draw;
}

void rendering_engine::start(std::shared_ptr<box2d> b2d)
{
	_d3d->init_d3d();
	_d3d_device = _d3d->get_d3d_device();
	_d3d_sprite = _d3d->get_d3d_sprite();

	IDirect3DTexture9* background_image(nullptr);
	HR(D3DXCreateTextureFromFile(
		_d3d_device.get(),
		_path_to_background_image.c_str(),
		&background_image));
	_background_image = create_com_pointer(background_image);

	IDirect3DTexture9* shadow_map(nullptr);
	HR(_d3d_device->CreateTexture(
		_d3d->get_backbuffer_width(), _d3d->get_back_buffer_height(),
		1,
		D3DUSAGE_RENDERTARGET,
		D3DFMT_X8R8G8B8,
		D3DPOOL_DEFAULT,
		&shadow_map,
		nullptr));
	_shadow_map = create_com_pointer(shadow_map);

	_sgr.reset(new shadow_geometry_renderer(_d3d_device));

	ID3DXLine* d3d_line(nullptr);
	HR(D3DXCreateLine(_d3d_device.get(), &d3d_line));
	_d3d_line = create_com_pointer(d3d_line);

	_b2d_debug_draw.reset(new b2d_debug_draw(_d3d_line));
	_b2d_debug_draw->SetFlags(b2Draw::e_aabbBit | b2Draw::e_shapeBit);
	b2d->set_debug_draw(_b2d_debug_draw);

	b2d->set_timestep(1.0f / static_cast<float>(_d3d->get_refresh_rate_hz()));

	_create_timer_font();

	const float PROJECTION_NEAR_PLANE = 1.0f;
	const float PROJECTION_FAR_PLANE = 2000.0f;

	// Build an orthographic projection matrix.
	D3DXMatrixOrthoLH(
		&_projection_matrices[0],
		b2d->pixels_to_meters(_d3d->get_backbuffer_width()),
		b2d->pixels_to_meters(_d3d->get_back_buffer_height()),
		PROJECTION_NEAR_PLANE,
		PROJECTION_FAR_PLANE);

	// Build a perspective projection matrix.
	D3DXMatrixPerspectiveFovLH(
		&_projection_matrices[1],
		D3DXToRadian(_d3d->get_vertical_fov()),
		_d3d->get_aspect_ratio(),
		PROJECTION_NEAR_PLANE,
		PROJECTION_FAR_PLANE);

	// Default to the orthographic projection because that's the way
	// 2d games are typically rendered.
	_current_projection_matrix = _projection_matrices.begin();

	_tqr.reset(new tex_quad_renderer(_d3d_device, b2d->get_pixels_per_meter()));
}

void rendering_engine::render_scene(std::shared_ptr<box2d> b2d)
{
	for(int i = 0; i < _point_lights.size(); ++i)
		_sgr->generate_shadows_for_hulls(_point_lights[i], i, _objects_to_render);

	IDirect3DSurface9* backbuffer(nullptr);
	_d3d_device->GetRenderTarget(0, &backbuffer);

	IDirect3DSurface9* shadow_map_surface(nullptr);
	_shadow_map->GetSurfaceLevel(0, &shadow_map_surface);

	_d3d_device->SetRenderTarget(0, shadow_map_surface);
	HR(_d3d_device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x00000000, 1.0f, 0));
	HR(_d3d_device->BeginScene());

	D3DXMATRIX view_projection = _camera->get_view() * (*_current_projection_matrix);
	_sgr->render(view_projection);

	HR(_d3d_device->EndScene());

	_d3d_device->SetRenderTarget(0, backbuffer);
	HR(_d3d_device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x0000ff, 1.0f, 0));
	HR(_d3d_device->BeginScene());

	// Render the background image.
	D3DXMATRIX identity;
	D3DXMatrixIdentity(&identity);
	auto bb_width_meters = _d3d->get_backbuffer_width() * b2d->get_meters_per_pixel();
	auto bb_height_meters = _d3d->get_back_buffer_height() * b2d->get_meters_per_pixel();
	const RECT bg_image_select = {0, 0, 800, 600};
	_tqr->render_texture(_background_image.get(), nullptr, identity, D3DXVECTOR3(1024.0f / 2.0f*b2d->get_meters_per_pixel(), 1024.0f / 2.0f*b2d->get_meters_per_pixel(), 5.0f));

	for(auto& go : _objects_to_render)
	{
		D3DXMATRIX transform;
		D3DXMatrixIdentity(&transform);
		//D3DXMatrixRotationZ(&transform, 0.5f);
		auto b2d_transform = go->get_physics_body()->GetTransform();
		transform(0, 0) = b2d_transform.q.GetXAxis()(0);
		transform(0, 1) = b2d_transform.q.GetXAxis()(1);
		transform(1, 0) = b2d_transform.q.GetYAxis()(0);
		transform(1, 1) = b2d_transform.q.GetYAxis()(1);

		auto r = go->get_render_rect();
		auto b2dpos = go->get_physics_body()->GetPosition();
		_tqr->render_texture(go->get_texture(), r, transform, D3DXVECTOR3(b2dpos.x, b2dpos.y, go->get_position().z));

		/*auto hull = go->get_convex_hull();
		hull.push_back(hull[0]);
		HR(_d3d_line->DrawTransform(&hull[0], hull.size(), &view_projection, D3DCOLOR_XRGB(255, 255, 255)));*/
	}

	#if defined(_DEBUG)
	// Render the timer if we have one.
	if(_timer)
	{
		_d3d_sprite->Begin(D3DXSPRITE_ALPHABLEND);
		_render_timer();
		_d3d_sprite->End();
	}

	_b2d_debug_draw->SetTransform(view_projection);
	b2d->get_world()->DrawDebugData();
	#endif

	_tqr->draw(view_projection, _shadow_map.get());

	HR(_d3d_device->EndScene());
	HR(_d3d_device->Present(0, 0, 0, 0));

	_tqr->clear_point_lights();
	_point_lights.clear();
}

std::shared_ptr<IDirect3DDevice9> rendering_engine::get_d3ddevice()
{
	return _d3d_device;
}

void rendering_engine::add_game_object(game_object* go)
{
	_objects_to_render.push_back(go);
}

void rendering_engine::remove_game_object(game_object* go)
{
	auto go_pos = std::find(_objects_to_render.begin(), _objects_to_render.end(), go);
	_objects_to_render.erase(go_pos);
}

void rendering_engine::render_point_light(const point_light& light)
{
	_tqr->set_point_light(light);
	_point_lights.push_back(light);
}