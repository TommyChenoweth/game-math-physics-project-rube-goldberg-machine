#include "b2d_debug_draw.hpp"

#include "debug.hpp"

void b2d_debug_draw::_b2d_to_d3d_vectors(const b2Vec2* vertices, const unsigned number_of_vertices)
{
	if(_vertices.size() < number_of_vertices+1)
		_vertices.resize(number_of_vertices+1, D3DXVECTOR3(0.0f, 0.0f, -2.0f));

	for(unsigned i = 0; i < number_of_vertices; ++i)
	{
		_vertices[i].x = vertices[i].x;
		_vertices[i].y = vertices[i].y;
	}

	_vertices[number_of_vertices].x = _vertices[0].x;
	_vertices[number_of_vertices].y = _vertices[0].y;
}

void b2d_debug_draw::_b2d_to_d3d_color(const b2Color& color)
{
	_color = D3DCOLOR_XRGB(static_cast<int>(color.r * 255), static_cast<int>(color.g * 255), static_cast<int>(color.b * 255));
}

b2d_debug_draw::b2d_debug_draw(std::shared_ptr<ID3DXLine> d3d_line)
	: _d3d_line(d3d_line)
{
}

void b2d_debug_draw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	DrawPolygon(vertices, vertexCount, color);
}

void b2d_debug_draw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	_b2d_to_d3d_vectors(vertices, vertexCount);
	_b2d_to_d3d_color(color);
	HR(_d3d_line->DrawTransform(&_vertices[0], _vertices.size(), &_view_projection, _color));
}

void b2d_debug_draw::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color)
{
}

void b2d_debug_draw::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color)
{
}

void b2d_debug_draw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color)
{
	static D3DXVECTOR3 line_segment_vertices[2];

	line_segment_vertices[0].x = p1.x;
	line_segment_vertices[0].y = p1.y;
	line_segment_vertices[0].z = 0.0f;

	line_segment_vertices[1].x = p2.x;
	line_segment_vertices[1].y = p2.y;
	line_segment_vertices[1].z = 0.0f;

	_b2d_to_d3d_color(color);

	HR(_d3d_line->DrawTransform(line_segment_vertices, 2, &_view_projection, _color));
}

void b2d_debug_draw::DrawTransform(const b2Transform& xf)
{
}

void b2d_debug_draw::SetTransform(const D3DXMATRIX& transform)
{
	_view_projection = transform;
}