#pragma once

#include <d3dx9.h>
#include <memory>
#include <string>
#include <vector>

/** Measures the passage of time. */
class timer
{
private:
	float	_fps;  // Frames Per Second
	float	_mspf; // Milliseconds Per Frame

	// Timer Control
	bool	_paused;
	bool	_resumed;

	// Time Measurement
	__int64	_counts_this_frame;
	__int64	_counts_last_frame;
	float	_seconds_per_count;

	float	_dt;

	/** Get the current count from the performance counter. */
	__int64 _get_counts();

public:
	timer();

	/**	Get the delta time - i.e. the time elapsed, in milliseconds, while
	 *	rendering and updating the previous frame. */
	float get_dt() const;
	/** Get the number of frames processed over the last second. */
	float get_fps() const;
	/** Get the average number of milliseconds it took to process a frame
	 *  over the last second. */
	float get_mspf() const;

	/** Pause the timer */
	void pause();
	/** Resume the timer */
	void resume();
	/** Returns true if the timer is paused, and false otherwise. */
	bool is_paused() const;

	/**	Calculate the time elapsed during the previous frame, and update the
	 *	displayed text each second. A lot of the logic was Frank D. Luna's.
	 *	http://www.d3dcoder.net/d3d9c.htm */
	void update();
};