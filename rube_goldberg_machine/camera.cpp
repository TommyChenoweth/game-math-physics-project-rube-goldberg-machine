#include "camera.hpp"

/**
 *	Rebuild the camera's view matrix based on its current orientation
 *	and position.
 */
void camera::_build_view_matrix()
{
	D3DXMatrixLookAtLH(&_view_matrix, &_position, &(_position+_look), &_up);
}

void camera::_build_yaw_matrix(const float radians)
{
	const D3DXVECTOR3 world_up(0.0f, 1.0f, 0.0f);
	D3DXMatrixRotationAxis(&_yaw_matrix, &world_up, radians);
}

void camera::_build_pitch_matrix(const float radians)
{
	D3DXMatrixRotationAxis(&_pitch_matrix, &_right, radians);
}

void camera::_build_roll_matrix(const float radians)
{
	D3DXMatrixRotationAxis(&_roll_matrix, &_look, radians);
}

camera::camera(const D3DXVECTOR3& position)
	: _right(1.0f, 0.0f, 0.0f),
	  _up(0.0f, 1.0f, 0.0f),
	  _look(0.0f, 0.0f, 1.0f),
	  _position(position)
{
	D3DXMatrixIdentity(&_view_matrix);
	D3DXMatrixIdentity(&_yaw_matrix);
	D3DXMatrixIdentity(&_pitch_matrix);
	D3DXMatrixIdentity(&_roll_matrix);

	_build_view_matrix();
}

D3DXVECTOR3 camera::get_right_vector() const
{
	return _right;
}

D3DXVECTOR3 camera::get_up_vector() const
{
	return _up;
}

D3DXVECTOR3 camera::get_look_vector() const
{
	return _look;
}

D3DXVECTOR3 camera::get_position() const
{
	return _position;
}

void camera::set_position(const D3DXVECTOR3& position)
{
	_position = position;
	_build_view_matrix();
}

/**
 *	Rotate the camera about the world's up axis.
 */
void camera::yaw(const float radians)
{
	_build_yaw_matrix(radians);
	D3DXVec3TransformCoord(&_look, &_look, &_yaw_matrix);
	D3DXVec3TransformCoord(&_right, &_right, &_yaw_matrix);
	D3DXVec3TransformCoord(&_up, &_up, &_yaw_matrix);
	_build_view_matrix();
}

/**
 *	Rotate the camera about its right axis.
 */
void camera::pitch(const float radians)
{
	_build_pitch_matrix(radians);
	D3DXVec3TransformCoord(&_look, &_look, &_pitch_matrix);
	D3DXVec3TransformCoord(&_up, &_up, &_pitch_matrix);
	_build_view_matrix();
}

/**
 *	Rotate the camera about its look axis.
 */
void camera::roll(const float radians)
{
	_build_roll_matrix(radians);
	D3DXVec3TransformCoord(&_right, &_right, &_roll_matrix);
	D3DXVec3TransformCoord(&_up, &_up, &_roll_matrix);
	_build_view_matrix();
}

D3DXMATRIX camera::get_view() const
{
	return _view_matrix;
}