#pragma once

#include <Box2d/Box2D.h>

class game_contact_listener : public b2ContactListener
{
private:
public:
	void BeginContact(b2Contact *contact);
	void EndContact(b2Contact *contact);
	void PreSolve(b2Contact *contact, const b2Manifold *oldManifold);
	void PostSolve(b2Contact *contact, const b2ContactImpulse *impulse);
};