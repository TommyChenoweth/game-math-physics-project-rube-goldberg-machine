#include "ball_game_object.hpp"

#include <cmath>

game_object::hull_type ball_game_object::_do_get_hull_type() const
{
	return game_object::hull_type::convex_polygon;
}

convex_hull ball_game_object::_do_get_convex_hull() const
{
	convex_hull hull;

	auto pos = _do_get_position();
	D3DXMATRIX translate;
	D3DXMatrixTranslation(&translate, pos.x, pos.y, 0.0f);
	for(const auto& v : _hull)
	{
		D3DXVECTOR3 transformed_vertex;
		D3DXVec3TransformCoord(&transformed_vertex, &v, &translate);
		hull.push_back(transformed_vertex);
	}

	return hull;
}

circle_hull ball_game_object::_do_get_circle_hull() const
{
	return {D3DXVECTOR3(0.0f, 0.0f, 0.0f), 0.0f};
}

b2Body* ball_game_object::_do_get_physics_body()
{
	return _physics_body;
}

D3DXVECTOR3 ball_game_object::_do_get_position() const
{
	auto b2d_pos = _physics_body->GetPosition();
	D3DXVECTOR3 d3d_position(b2d_pos.x, b2d_pos.y, _position.z);
	return d3d_position;
}

const RECT* ball_game_object::_do_get_render_rect() const
{
	return nullptr;
}

ball_game_object::ball_game_object(const std::wstring& path_to_texture_file, bool glows, const D3DXVECTOR3& position, const float radius, b2World* world, IDirect3DDevice9* d3d_device)
: game_object(path_to_texture_file, d3d_device),
  _physics_body(nullptr),
  _position(position),
  _radius(radius)
{
	b2BodyDef body_def;
	body_def.type = b2_dynamicBody;
	body_def.position.Set(position.x, position.y);
	_physics_body = world->CreateBody(&body_def);

	b2CircleShape shape;
	shape.m_radius = radius;
	//shape.m_p = b2Vec2(position.x, position.y);

	b2FixtureDef fixture_def;
	fixture_def.shape = &shape;
	fixture_def.density = 1.0f;

	_physics_body->CreateFixture(&fixture_def);

	if(!glows)
	{
		const int resolution = 20;
		for(int i = 0; i < resolution; ++i)
		{
			D3DXVECTOR3 v(0.0f, 0.0f, 0.0f);

			auto theta = i * (2 * D3DX_PI / resolution);
			v.x = std::cos(theta);
			v.y = std::sin(theta);

			v *= radius - 0.01f;

			_hull.push_back(v);
		}
	}
}

void ball_game_object::attach_point_light(const point_light& light)
{
	_light = light;
}

point_light ball_game_object::get_point_light()
{
	auto ball_pos = _do_get_position();
	_light.pos_and_radius.x = ball_pos.x;
	_light.pos_and_radius.y = ball_pos.y;
	return _light;
}