#include "button.hpp"

game_object::hull_type button::_do_get_hull_type() const
{
	return game_object::hull_type::convex_polygon;
}

convex_hull button::_do_get_convex_hull() const
{
	return convex_hull();
}

circle_hull button::_do_get_circle_hull() const
{
	return {};
}

b2Body* button::_do_get_physics_body()
{
	return _physics_body;
}

D3DXVECTOR3 button::_do_get_position() const
{
	auto b2d_pos = _physics_body->GetPosition();
	return D3DXVECTOR3(b2d_pos.x, b2d_pos.y, _position.z);
}

button::button(const D3DXVECTOR3& position, b2World* world, IDirect3DDevice9* d3d_device)
: game_object(L"../textures/push_button.dds", d3d_device),
  _position(position),
  _physics_body(nullptr),
  _width(0.5f),
  _height(0.5f)
{
	b2BodyDef body_def;
	body_def.type = b2_staticBody;
	body_def.position.Set(position.x, position.y);
	_physics_body = world->CreateBody(&body_def);

	b2PolygonShape shape;
	shape.SetAsBox(_width / 2, _height / 2);

	b2FixtureDef fixture_def;
	fixture_def.shape = &shape;
	fixture_def.density = 1.0f;
	fixture_def.isSensor = true;

	_physics_body->CreateFixture(&fixture_def);
}