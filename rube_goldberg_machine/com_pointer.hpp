#pragma once

#include <d3d9.h>
#include <memory>

/**
 *	Returns a smart pointer that will correctly release the passed DirectX 
 *	resource upon destruction.
 *
 *	The idea for this function came from the boost documentation. Specifically,
 *	it came from here: http://www.boost.org/doc/libs/1_51_0/libs/smart_ptr/sp_techniques.html
 */
template <typename T>
std::shared_ptr<T> create_com_pointer(T* com_object)
{
	return std::shared_ptr<T> (com_object, [](T* p){p->Release();});
}