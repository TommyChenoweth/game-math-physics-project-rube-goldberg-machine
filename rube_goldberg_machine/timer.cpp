#include "timer.hpp"

#include "com_pointer.hpp"
#include "debug.hpp"

#include <iostream>
#include <strsafe.h>
#include <Windows.h>

__int64 timer::_get_counts()
{
	/* This function is used in the class initialization list. DO NOT USE MEMBER
	   VARIABLES HERE! */

	__int64 counts(0);
	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&counts));
	return counts;
}

timer::timer()
	: _fps(0.0f),
	  _mspf(0.0f),
	  _paused(false),
	  _resumed(false),
	  _counts_this_frame(_get_counts()),
	  _counts_last_frame(_counts_this_frame),
	  _seconds_per_count(0.0f),
	  _dt(0.0f)
{
	__int64 counts_per_second(0);
	QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&counts_per_second));
	_seconds_per_count = 1.0f/static_cast<float>(counts_per_second);
}

float timer::get_dt() const
{
	return _dt;
}

float timer::get_fps() const
{
	return _fps;
}

float timer::get_mspf() const
{
	return _mspf;
}

void timer::pause()
{
	_paused = true;
	std::cout << "Pausing..." << std::endl;
}

void timer::resume()
{
	_paused = false;
	_resumed = true;
	std::cout << "Resuming..." << std::endl;
}

bool timer::is_paused() const
{
	return _paused;
}

void timer::update()
{
	static float time_elapsed(0.0f);
	static int number_of_frames(0);

	if(_resumed)
	{
		_counts_this_frame = _get_counts();
		_counts_last_frame = _counts_this_frame;
		_resumed = false;
	}
	else
	{
		_counts_last_frame = _counts_this_frame;
		_counts_this_frame = _get_counts();
	}

	_dt = (_counts_this_frame - _counts_last_frame) * _seconds_per_count;
	time_elapsed += _dt;

	++number_of_frames;

	bool at_least_one_second_elapsed(1.0f <= time_elapsed);
	if(at_least_one_second_elapsed)
	{
		time_elapsed = 0;

		_fps = static_cast<float>(number_of_frames);
		number_of_frames = 0;

		_mspf = 1000.0f / _fps;
	}
}