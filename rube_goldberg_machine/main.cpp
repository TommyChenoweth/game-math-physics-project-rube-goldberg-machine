#include "game.hpp"

#include <iostream>
#include <Windows.h>

void create_and_attach_console();

int WINAPI wWinMain(HINSTANCE instance_handle, HINSTANCE instance_handle_prev, LPWSTR command_line_args, int show)
{
	// Enable run-time memory check and console for debug builds.
#if defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	create_and_attach_console();
#endif

	std::shared_ptr<game> game(new game(instance_handle));
	game->run();

	return 0;
}

void create_and_attach_console()
{
	// Create a console.
	AllocConsole();

	// Attach the created console to this application.
	AttachConsole(GetCurrentProcessId());

	// Redirect output to the attached console.
	FILE* stdout_stream(nullptr);
	freopen_s(&stdout_stream, "CON", "w", stdout);
	FILE* stderr_stream(nullptr);
	freopen_s(&stderr_stream, "CON", "w", stderr);

	std::ios::sync_with_stdio();

	// Change the console's title to more accurately reflect its purpose.
	SetConsoleTitle(L"Debug Console");

	// Disable the console's close button as this seems to cause the application
	// to close ungracefully.
	RemoveMenu(GetSystemMenu(GetConsoleWindow(), false), SC_CLOSE, MF_BYCOMMAND);
	DrawMenuBar(GetConsoleWindow());
}