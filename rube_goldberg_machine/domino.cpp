#include "domino.hpp"

b2FixtureDef domino::_create_fixture_definition(const b2PolygonShape& shape)
{
	b2FixtureDef fixture_def;
	fixture_def.shape = &shape;
	fixture_def.density = 1.0f;
	fixture_def.friction = 1.0f;
	return fixture_def;
}

const RECT* domino::_do_get_render_rect() const
{
	return &_render_rect;
}

domino::domino(const D3DXVECTOR3& position, b2World* world, const float pixels_per_meter, const float width, const float height, IDirect3DDevice9* d3d_device)
: rectangular_game_object(position, world, width / 2, height / 2, L"../textures/domino.dds", d3d_device),
_width(width),
_height(height)
{
	_render_rect = {0, 0, width*pixels_per_meter, height*pixels_per_meter};
	rebuild_physics_body(position, width / 2, height / 2, world);
}