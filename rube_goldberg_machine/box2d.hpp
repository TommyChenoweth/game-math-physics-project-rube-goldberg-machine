#pragma once

#include <Box2D/Box2D.h>
#include <memory>

class b2d_debug_draw;

class box2d
{
private:
	b2Vec2								_gravity;
	std::shared_ptr<b2World>			_world;

	float								_world_width_meters;
	float								_world_height_meters;

	float32								_timestep;
	int32								_velocity_iterations;
	int32								_position_iterations;

	float32								_pixels_per_meter;
	float32								_meters_per_pixel;

public:
	box2d(const float pixels_per_meter, const float world_width_meters, const float world_height_meters);

	std::shared_ptr<b2World> get_world();

	float get_pixels_per_meter() const;
	float get_meters_per_pixel() const;

	void set_timestep(const float timestep);

	void set_debug_draw(std::shared_ptr<b2d_debug_draw> b2d_debug_draw);

	void step();

	float pixels_to_meters(const float pixels) const;
	float meters_to_pixels(const float meters) const;
};