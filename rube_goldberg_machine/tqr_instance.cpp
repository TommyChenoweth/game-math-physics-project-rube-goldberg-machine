#include "tqr_instance.hpp"

tqr_instance::tqr_instance(const D3DXVECTOR3& quad_position, const D3DXVECTOR4& quad_rect)
	: quad_position(quad_position),
	  quad_rect(quad_rect)
{
}

tqr_instance::tqr_instance()
	: quad_position(0.0f, 0.0f, 0.0f),
	  quad_rect(0.0f, 0.0f, 1.0f, 1.0f)
{
}