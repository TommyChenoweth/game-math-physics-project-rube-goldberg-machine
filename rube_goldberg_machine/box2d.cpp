#include "box2d.hpp"

#include "b2d_debug_draw.hpp"

box2d::box2d(const float pixels_per_meter, const float world_width_meters, const float world_height_meters)
	: _gravity(0.0f, -10.0f),
	  _world(new b2World(_gravity)),
	  _world_width_meters(world_width_meters),
	  _world_height_meters(world_height_meters),
	  _timestep(1.0f / 60.0f), // A smaller timestep works better with high variable fps.
	  _velocity_iterations(6),
	  _position_iterations(2),
	  _pixels_per_meter(pixels_per_meter),
	  _meters_per_pixel(1.0f / _pixels_per_meter)
{
	b2Vec2 chain_vectors[4] = 
	{
		b2Vec2(0.0f, 0.0f), 
		b2Vec2(0.0f, _world_height_meters), 
		b2Vec2(_world_width_meters, _world_height_meters), 
		b2Vec2(_world_width_meters, 0.0f)
	};

	b2BodyDef world_boundary_definition;
	world_boundary_definition.position.Set(0.0f, 0.0f);
	auto world_boundary_body = _world->CreateBody(&world_boundary_definition); // The b2World will delete this automatically.

	b2ChainShape chain_shape;
	chain_shape.CreateLoop(chain_vectors, 4);
	world_boundary_body->CreateFixture(&chain_shape, 0.0f);
}

std::shared_ptr<b2World> box2d::get_world()
{
	return _world;
}

float box2d::get_pixels_per_meter() const
{
	return _pixels_per_meter;
}

float box2d::get_meters_per_pixel() const
{
	return _meters_per_pixel;
}

void box2d::set_timestep(const float timestep)
{
	_timestep = timestep;
}

void box2d::set_debug_draw(std::shared_ptr<b2d_debug_draw> b2d_debug_draw)
{
	assert(_world && b2d_debug_draw);
	_world->SetDebugDraw(b2d_debug_draw.get());
}

void box2d::step()
{
	_world->Step(_timestep, _velocity_iterations, _position_iterations);
}

float box2d::pixels_to_meters(const float pixels) const
{
	return pixels * _meters_per_pixel;
}

float box2d::meters_to_pixels(const float meters) const
{
	return meters * _pixels_per_meter;
}