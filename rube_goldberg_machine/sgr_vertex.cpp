#include "sgr_vertex.hpp"

#include "com_pointer.hpp"
#include "debug.hpp"

std::shared_ptr<IDirect3DVertexDeclaration9> sgr_vertex::_vertex_declaration;

sgr_vertex::sgr_vertex(const D3DXVECTOR3& position, const D3DXVECTOR4& color)
	: position(position),
	  color(color)
{
}

sgr_vertex::sgr_vertex()
	: position(D3DXVECTOR3(0.0f, 0.0f, 0.0f)),
	  color(0.0f, 0.0f, 0.0f, 0.0f)
{
}

void sgr_vertex::initialize_vertex_declaration(std::shared_ptr<IDirect3DDevice9> d3d_device)
{
	if(_vertex_declaration)
		return;

	// Describe the format of the vertices that we'll be using.
	D3DVERTEXELEMENT9 vertex_position[] =
	{
		{0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 12, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
		D3DDECL_END()
	};

	// Create a vertex declaration based on the format we described above.
	IDirect3DVertexDeclaration9* vertex_declaration(nullptr);
	HR(d3d_device->CreateVertexDeclaration(vertex_position, &vertex_declaration));
	_vertex_declaration = create_com_pointer(vertex_declaration);
}

std::shared_ptr<IDirect3DVertexDeclaration9> sgr_vertex::get_vertex_declaration()
{
	return _vertex_declaration;
}