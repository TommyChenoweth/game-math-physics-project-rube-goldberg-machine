#include "direct3d.hpp"

#include "com_pointer.hpp"
#include "debug.hpp"
#include "window.hpp"

#include <iostream>

HRESULT direct3d::_create_d3d_object()
{
	_d3d_object = create_com_pointer(Direct3DCreate9(D3D_SDK_VERSION));
	if(!_d3d_object)
	{
		std::cerr << L"Failed to create an IDirect3D9 object." << std::endl;
		return E_FAIL;
	}
	return D3D_OK;
}

HRESULT direct3d::_verify_hal_and_format_support()
{
	D3DDISPLAYMODE mode;
	_d3d_object->GetAdapterDisplayMode(_adapter, &mode);

	_refresh_rate = mode.RefreshRate;

	// Verify windowed support of HAL. We'll use whatever
	// format that the adapter is currently using.
	HRESULT windowed_support_of_hal = _d3d_object->CheckDeviceType(
		_adapter,
		_device_type,
		mode.Format,
		mode.Format,
		true);

	if(FAILED(windowed_support_of_hal))
	{
		display_error(windowed_support_of_hal);
		return E_FAIL;
	}

	// Verify fullscreen support of HAL and format.
	HRESULT fullscreen_support_of_hal = _d3d_object->CheckDeviceType(
		_adapter,
		_device_type,
		_adapter_format,
		_backbuffer_format,
		false);

	if(FAILED(fullscreen_support_of_hal))
	{
		display_error(fullscreen_support_of_hal);
		return E_FAIL;
	}

	return D3D_OK;
}

HRESULT direct3d::_verify_hardware_vertex_processing()
{
	D3DCAPS9 caps;
	HRESULT get_device_caps = _d3d_object->GetDeviceCaps(_adapter, _device_type, &caps);

	if(FAILED(get_device_caps))
	{
		display_error(get_device_caps);
		return E_FAIL;
	}

	// If the device supports transform and light, we can use whichever
	// vertex processing we please.
	if(caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
		_device_behavior_flags |= _requested_vertex_processing;
	else
	{
		// Hardware vertex processing isn't available. The only vertex
		// processing we can use is software.
		std::cerr << L"Hardware vertex processing isn't available. Using software instead.";
		_device_behavior_flags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;
	}

	bool hardware_supports_pure_device((caps.DevCaps & D3DDEVCAPS_PUREDEVICE) != 0);
	bool hardware_vp_requested((_device_behavior_flags & D3DCREATE_HARDWARE_VERTEXPROCESSING) != 0);
	if(hardware_supports_pure_device && hardware_vp_requested)
	{
		_device_behavior_flags |= D3DCREATE_PUREDEVICE;
	}

	return D3D_OK;
}

bool direct3d::_shaders_are_supported()
{
	D3DCAPS9 caps;
	HR(_d3d_object->GetDeviceCaps(_adapter, _device_type, &caps));

	if(caps.VertexShaderVersion < D3DVS_VERSION(2, 0))
		return false;

	if(caps.PixelShaderVersion < D3DVS_VERSION(2, 0))
		return false;

	return true;
}

void direct3d::_set_present_params()
{
	_present_params.BackBufferWidth				= _backbuffer_width;
	_present_params.BackBufferHeight			= _backbuffer_height;
	_present_params.BackBufferFormat			= D3DFMT_UNKNOWN;
	_present_params.BackBufferCount				= 1;
	_present_params.MultiSampleType				= D3DMULTISAMPLE_NONE;
	_present_params.MultiSampleQuality			= 0;
	_present_params.SwapEffect					= D3DSWAPEFFECT_DISCARD;
	_present_params.hDeviceWindow = _window->get_hwnd();
	_present_params.Windowed					= true;
	_present_params.EnableAutoDepthStencil		= true;
	_present_params.AutoDepthStencilFormat		= D3DFMT_D24S8;
	_present_params.Flags						= 0;
	_present_params.FullScreen_RefreshRateInHz	= D3DPRESENT_RATE_DEFAULT;
	#if defined(DEBUG) | defined(_DEBUG)
	//_present_params.PresentationInterval			= D3DPRESENT_INTERVAL_IMMEDIATE;
	_present_params.PresentationInterval			= D3DPRESENT_INTERVAL_DEFAULT;
	#else
	//_present_params.PresentationInterval			= D3DPRESENT_INTERVAL_IMMEDIATE;
	_present_params.PresentationInterval			= D3DPRESENT_INTERVAL_DEFAULT;
	#endif
}

HRESULT direct3d::_create_device_interface()
{
	IDirect3DDevice9* d3ddevice = nullptr;
	HRESULT createdevice_result = _d3d_object->CreateDevice(
		_adapter,
		_device_type,
		_window->get_hwnd(),
		_device_behavior_flags,
		&_present_params,
		&d3ddevice);
	_d3d_device = create_com_pointer(d3ddevice);

	if(FAILED(createdevice_result))
	{
		display_error(createdevice_result);
		return E_FAIL;
	}

	return D3D_OK;
}

void direct3d::_create_sprite_object()
{
	ID3DXSprite* sprite(nullptr);
	HR(D3DXCreateSprite(_d3d_device.get(), &sprite));
	_d3d_sprite = create_com_pointer(sprite);
}

direct3d::direct3d(std::shared_ptr<window> window)
	: _d3d_object(nullptr),
	  _d3d_device(nullptr),
	  _d3d_sprite(nullptr),
	  _adapter(D3DADAPTER_DEFAULT),
	  _requested_vertex_processing(D3DCREATE_HARDWARE_VERTEXPROCESSING),
	  _device_behavior_flags(0),
	  _device_type(D3DDEVTYPE_HAL),
	  _adapter_format(D3DFMT_X8R8G8B8),
	  _backbuffer_format(D3DFMT_X8R8G8B8),
	  _backbuffer_width(800.0f),
	  _backbuffer_height(600.0f),
	  _vertical_fov_degrees(22.0f),
	  _aspect_ratio(_backbuffer_width/_backbuffer_height),
	  _window(window)
{
	ZeroMemory(&_present_params, sizeof(_present_params));
}

void direct3d::init_d3d()
{
	// These functions return HRESULTS because it may be possible
	// to recover from the errors; However, I'm not sure how to do it right
	// now. The return types are place holders for that. Also, the order of
	// these function calls is important, so don't change them.
	if(FAILED(_create_d3d_object()))
		PostQuitMessage(0);

	if(FAILED(_verify_hal_and_format_support()))
		PostQuitMessage(0);

	if(FAILED(_verify_hardware_vertex_processing()))
		PostQuitMessage(0);

	// If we check shader support before selecting hardware vertex processing,
	// could we request software vertex processing and run the game? I'll test
	// this later.
	if(!_shaders_are_supported())
	{
		MessageBox(
			_window->get_hwnd(),
			L"Your graphics card doesn't support the required shaders.", 
			L"ERROR", 
			MB_OK);
		PostQuitMessage(0);
	}

	_set_present_params();

	if(FAILED(_create_device_interface()))
		PostQuitMessage(0);

	_create_sprite_object();
}

bool direct3d::started() const
{
	return !!_d3d_object;
}

std::shared_ptr<IDirect3D9> direct3d::get_d3d_object()
{
	return _d3d_object;
}

std::shared_ptr<IDirect3DDevice9> direct3d::get_d3d_device()
{
	return _d3d_device;
}

std::shared_ptr<ID3DXSprite> direct3d::get_d3d_sprite()
{
	return _d3d_sprite;
}

float direct3d::get_backbuffer_width() const
{
	return _backbuffer_width;
}

float direct3d::get_back_buffer_height() const
{
	return _backbuffer_height;
}

float direct3d::get_vertical_fov() const
{
	return _vertical_fov_degrees;
}

float direct3d::get_aspect_ratio() const
{
	return  _aspect_ratio;
}

UINT direct3d::get_refresh_rate_hz() const
{
	return _refresh_rate;
}