#pragma once

#include <array>
#include <memory>
#include <Windows.h>

class window;

/**
 *  Responsible for obtaining player input, and making that input accessible
 *  to the developers.
 *  
 *  poll_input_devices must be called once per frame to obtain input data.
 */
class input
{
private:
	typedef std::array<BYTE, 256> keys;

	// Key State
	keys _key_state_curr;
	keys _key_state_prev;

	// Mouse/Cursor State
	POINT	_mouse_position_curr;
	POINT	_mouse_position_prev;
	POINT	_mouse_position_delta;
	bool	_mouse_position_locked;

	std::shared_ptr<window> _window;

	/**	Copy the current device states to the previous device states.*/
	void _current_to_previous();

	/**	Update the state of all keys on the keyboard. */
	void _poll_keyboard();
	/**	Update the state of the mouse and cursor. */
	void _poll_mouse();

public:
	input(std::shared_ptr<window> window);

	/**	Accepts a windows virtual-key code, and returns true if the key is up. */
	bool is_key_up(const int key) const;
	/**	Accepts a windows virtual-key code, and returns true if the key is down. */
	bool is_key_down(const int key) const;
	/**	Accepts a windows virtual-key code, and returns true if the key is pressed.
	 *	A key is considered pressed when its state changes from up to down. */
	bool is_key_pressed(const int key) const;
	/**	Accepts a windows virtual-key code, and returns true if the key is released.
	 *	A key is considered released when its state changes from down to up. */
	bool is_key_released(const int key) const;

	POINT get_mouse_position() const;

	POINT get_mouse_position_delta() const;

	/**	Hides the cursor, and keeps it locked to one position. Mouse position deltas
	 *	are still calculated while the mouse is locked. */
	void lock_mouse();
	/**	Reveals and unlocks the cursor if it was locked. */
	void unlock_mouse();
	bool is_mouse_locked() const;

	/**	This should be called once each frame to get the state of the mouse
	 *	and keyboard. */
	void poll_input_devices();
};