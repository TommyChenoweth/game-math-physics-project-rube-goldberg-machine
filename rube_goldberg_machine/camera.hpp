#pragma once

#include <d3dx9math.h>

class camera
{
private:
	// View Matrix
	D3DXMATRIX	_view_matrix;
	D3DXVECTOR3	_right;
	D3DXVECTOR3	_up;
	D3DXVECTOR3	_look;
	D3DXVECTOR3	_position;

	// Transformation Matrices
	D3DXMATRIX _yaw_matrix;
	D3DXMATRIX _pitch_matrix;
	D3DXMATRIX _roll_matrix;

	void _build_view_matrix();
	void _build_yaw_matrix(const float radians);
	void _build_pitch_matrix(const float radians);
	void _build_roll_matrix(const float radians);

public:
	camera(const D3DXVECTOR3& position);

	D3DXVECTOR3 get_right_vector() const;
	D3DXVECTOR3 get_up_vector() const;
	D3DXVECTOR3 get_look_vector() const;

	D3DXVECTOR3 get_position() const;
	void set_position(const D3DXVECTOR3& position);

	void yaw(const float radians);
	void pitch(const float radians);
	void roll(const float radians);

	D3DXMATRIX get_view() const;
};