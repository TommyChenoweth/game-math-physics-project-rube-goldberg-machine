#pragma once

#include "game_object.hpp"

class button : public game_object
{
private:
	D3DXVECTOR3 _position;
	b2Body*		_physics_body;
	float		_width;
	float		_height;

	virtual hull_type _do_get_hull_type() const;
	virtual convex_hull _do_get_convex_hull() const;
	virtual circle_hull _do_get_circle_hull() const;

	virtual b2Body* _do_get_physics_body();

	virtual D3DXVECTOR3 _do_get_position() const;

public:
	button(const D3DXVECTOR3& position, b2World* world, IDirect3DDevice9* d3d_device);

};