#pragma once

#include "point_light.hpp"

#include <algorithm>
#include <array>
#include <d3d9.h>
#include <D3dx9math.h>
#include <irrKlang.h>
#include <memory>
#include <string>
#include <vector>
#include <Windows.h>

class ball_game_object;
class button;
class box2d;
class camera;
class domino;
class input;
class peg;
class platform;
class rendering_engine;
class timer;
class window;

/** @brief Utilizes the various other classes in this project to create a game.
 */
class game
{
private:
	float								_world_width_meters;
	float								_world_height_meters;

	std::shared_ptr<window>					_window;
	std::shared_ptr<camera>					_camera;
	std::shared_ptr<box2d>					_b2d;
	std::shared_ptr<input>					_input;
	std::shared_ptr<timer>					_timer;
	std::shared_ptr<rendering_engine>		_rendering_engine;

	std::shared_ptr<platform>				_start_vertical;
	std::shared_ptr<platform>				_start_horizontal;
	std::shared_ptr<platform>				_start_catch_vertical;
	std::shared_ptr<platform>				_start_catch_horizontal;

	std::shared_ptr<platform>				_final_slide_01;
	std::shared_ptr<platform>				_final_slide_02;
	std::shared_ptr<platform>				_final_slide_03;
	std::shared_ptr<platform>				_final_slide_04;

	std::shared_ptr<ball_game_object>		_first_ball;
	std::shared_ptr<ball_game_object>		_second_ball;
	std::shared_ptr<ball_game_object>		_third_ball;

	std::shared_ptr<button>					_first_button;
	std::shared_ptr<button>					_second_button;
	std::shared_ptr<button>					_final_button;

	int										_peg_slant_resolution;
	std::vector<std::shared_ptr<peg>>		_pegs;

	std::array<std::shared_ptr<domino>, 7>	_dominos;

	point_light								_top_left_light;

	bool									_stage_two_started;
	bool									_stage_two_init;
	bool									_stage_three_started;
	bool									_stage_three_init;
	bool									_final_stage_init;

	irrklang::ISoundEngine*					_sound_engine;

	void _set_custom_wnd_proc();

	void _build_game_objects();
	void _place_pegs();
	void _place_dominos();
	void _stage_two();
	void _stage_three();

	void _reset_stage();

public:
	/** @param instance_handle The instance handle that was passed into winmain.
	 */
	game(HINSTANCE instance_handle);

	/** @brief Starts the game's main loop.
	 */
	void run();

	timer* get_timer();
};

extern game* g_game;