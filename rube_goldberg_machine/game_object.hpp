#pragma once

#include <Box2D\Box2D.h>
#include <d3dx9.h>
#include <memory>
#include <string>
#include <vector>

typedef std::vector<D3DXVECTOR3> convex_hull;
struct circle_hull
{
	D3DXVECTOR3 center;
	float		radius;
};

class game_object
{
public:
	enum class hull_type
	{
		convex_polygon,
		circle
	};

private:
	std::shared_ptr<IDirect3DTexture9>	_texture;

	virtual hull_type _do_get_hull_type() const = 0;
	virtual convex_hull _do_get_convex_hull() const = 0;
	virtual circle_hull _do_get_circle_hull() const = 0;

	virtual b2Body* _do_get_physics_body() = 0;

	virtual D3DXVECTOR3 _do_get_position() const = 0;
	virtual const RECT* _do_get_render_rect() const;

	virtual IDirect3DTexture9* _create_texture_from_file(const std::wstring& path_to_texure_file, IDirect3DDevice9* d3d_device);

public:
	game_object(const std::wstring& path_to_texure_file, IDirect3DDevice9* d3d_device);

	hull_type get_hull_type() const;
	convex_hull get_convex_hull() const;
	circle_hull get_circle_hull() const;

	b2Body* get_physics_body();

	D3DXVECTOR3 get_position() const;
	IDirect3DTexture9* get_texture() const;
	const RECT* get_render_rect() const;
};