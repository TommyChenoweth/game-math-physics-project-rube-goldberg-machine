uniform extern float4x4 g_view_projection;

struct PointLight
{
    float4 pos_and_radius;
    // float3 position;
    // float radius;
};
uniform extern PointLight g_point_lights[3];

struct VertexOutput
{
    float4 homogenous_position    : POSITION0;
    float4 color                  : TEXCOORD0;
};

VertexOutput ObjectToProjectionSpace(float3 world_position : POSITION0, float4 color : TEXCOORD0)
{
    VertexOutput v_out = (VertexOutput)0;
    
    v_out.color = color;
    
    // Transform position from world space to view space.
    v_out.homogenous_position = mul(float4(world_position, 1.0f), g_view_projection);
    
    return v_out;
}

float4 ApplyTexture(float4 color : TEXCOORD0) : COLOR
{
    float4 shadow_channel;
    //shadow_channel.rgba = float4(0, 1, 1, 1);
    return color;
    
    return shadow_channel;
}

technique TQRenderTech
{
    pass P0
    {
        vertexShader = compile vs_3_0 ObjectToProjectionSpace();
        pixelShader = compile ps_3_0 ApplyTexture();
        AlphaBlendEnable = true;
        SrcBlend                 = ONE;
        DestBlend                = ONE;
    }
}