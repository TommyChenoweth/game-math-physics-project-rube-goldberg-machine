uniform extern float4x4 g_view_projection;
uniform extern texture  g_texture;
uniform extern float    g_texture_width;
uniform extern float    g_texture_height;
uniform extern float4   g_ambient_light;
uniform extern texture  g_shadow_map;

struct PointLight
{
    float4 pos_and_radius;
    float4 color;
    // float3 position;
    // float radius;
};
uniform extern PointLight g_point_light_01;
uniform extern PointLight g_point_light_02;
uniform extern PointLight g_point_light_03;
//uniform extern PointLight g_point_lights[10];

sampler texture_sampler = sampler_state
{
    Texture = <g_texture>;
    
    // I think we'll be using pixel art, so the point filter makes the most sense.
    Filter = MIN_MAG_MIP_POINT;
    
	AddressU  = MIRROR;
    AddressV  = MIRROR;
};

sampler shadow_sampler = sampler_state
{
    Texture = <g_shadow_map>;
    
    Filter = MIN_MAG_MIP_POINT;
    
    AddressU  = WRAP;
    AddressV  = WRAP;
};

struct VertexOutput
{
    float4 homogenous_position    : POSITION0;
    float2 texture_coordinates    : TEXCOORD0;
    float3 world_position         : TEXCOORD2;
    float2 shadow_coordinate      : TEXCOORD3;
};

VertexOutput ObjectToProjectionSpace(float3 world_position : POSITION0, float2 texture_coordinates : TEXCOORD0,
                                     float3 unused_position : POSITION1, float4 unused_rect : TEXCOORD1)
{
    VertexOutput v_out = (VertexOutput)0;
    
    v_out.texture_coordinates = texture_coordinates;
    v_out.world_position = world_position;
    
    // Transform position from world space to view space.
    v_out.homogenous_position = mul(float4(world_position, 1.0f), g_view_projection);
    
    v_out.shadow_coordinate = float2(world_position.x/12.5, 1.0 - world_position.y/9.375);
    
    return v_out;
}

float4 ApplyTexture(float2 texture_coordinate : TEXCOORD0, float3 world_position : TEXCOORD2, float2 shadow_coordinate : TEXCOORD3) : COLOR
{
    float4  texture_color = tex2D(texture_sampler, texture_coordinate);
    
    float width = 800.0f/64.0f;
    float height = 600.0f/64.0f;
    
    float4 shadow_data = tex2D(shadow_sampler, shadow_coordinate);
    
    ///* This attenuation function comes from http://blog.slindev.com/2011/01/10/natural-light-attenuation/ */
    
    float attenuation = 0.0f;
    float light_01_intensity = 1.0f - shadow_data.r;
    float light_01_distance = distance(g_point_light_01.pos_and_radius.xyz, world_position);
    attenuation += saturate(1 - light_01_distance / g_point_light_01.pos_and_radius.w) * light_01_intensity;
    
    float light_02_intensity = 1.0f - shadow_data.g;
    float light_02_distance = distance(g_point_light_02.pos_and_radius.xyz, world_position);
    attenuation += saturate(1 - light_02_distance / g_point_light_02.pos_and_radius.w) * light_02_intensity;
    
    float light_03_intensity = 1.0f - shadow_data.b;
    float light_03_distance = distance(g_point_light_03.pos_and_radius.xyz, world_position);
    attenuation += saturate(1 - light_03_distance / g_point_light_03.pos_and_radius.w) * light_03_intensity;
    
    attenuation = min(attenuation, 1.0f);
    
    return float4(texture_color * (g_ambient_light + pow(attenuation, 2)));
}

technique TQRenderTech
{
    pass P0
    {
        vertexShader = compile vs_3_0 ObjectToProjectionSpace();
        pixelShader = compile ps_3_0 ApplyTexture();
        AlphaTestEnable = true;
        AlphaFunc = GreaterEqual;
        AlphaRef = 220;
    }
}